import re

FILENAME = 'everybody_codes_e2024_q02_p1.txt'
# FILENAME = "example_p1.txt"


def main() -> None:
    with open(FILENAME, "r") as f:
        lines = f.readlines()
    runes = [x.strip() for x in lines[0].split(":")[1].split(",")]
    print(runes)
    for line in lines[2:]:
        total_cases = 0
        print(line)
        for rune in runes:
            cases = len(re.findall('(?=({0}))'.format(rune), line))
            print(rune, cases)
            total_cases += cases
        print(total_cases)


if __name__ == "__main__":
    print(main())
