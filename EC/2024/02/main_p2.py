FILENAME = "everybody_codes_e2024_q02_p2.txt"
# FILENAME = "example_p2.txt"


def window_count(line: str, runes: list[str]) -> set[int]:
    characters: set[int] = set()
    for rune in runes:
        rune_pos = 0
        idx = 0
        while rune_pos >= 0:
            rune_pos = line.find(rune, idx)
            if rune_pos >= 0:
                characters.update(list(range(rune_pos, rune_pos + len(rune))))
                idx = rune_pos + 1
            # print("Iter", line[idx:], rune, characters, rune_pos)
        # print("Done with run", line, rune, characters)
    return characters


def main() -> int:
    with open(FILENAME, "r") as f:
        lines = f.readlines()
    runes = [x.strip() for x in lines[0].split(":")[1].split(",")]
    print("Runes", runes)
    total = 0
    for line in lines[2:]:
        cases = window_count(line, runes)
        cases_reverse = window_count(line, [rune[::-1] for rune in runes])
        total_cases = cases | cases_reverse
        total_line = len(total_cases)
        # print(
        #     "FINAL LINE",
        #     line,
        #     runes,
        #     cases,
        #     cases_reverse,
        #     f"{total_cases=}",
        #     f"{total_line=}",
        # )
        total += total_line
    return total


if __name__ == "__main__":
    print(main())
