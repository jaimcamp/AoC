FILENAME = "everybody_codes_e2024_q01_p2.txt"
# FILENAME = "test_p2.txt"
dict_values = {"A": 0, "B": 1, "C": 3, "D": 5, "x": -2}


def convert_enemy(line: str) -> int:
    pairs = [line[x : x + 2] for x in range(0, len(line), 2)]
    total = 0
    for pair in pairs:
        if pair != "xx":
            total += sum([dict_values[element] for element in pair]) + 2
            print(total, pair, sum([dict_values[element] for element in pair]) + 2)
    return total


def main() -> int:
    with open(FILENAME, "r") as f:
        line = f.readline().strip()
        value_line = convert_enemy(line)
        return value_line


if __name__ == "__main__":
    print(main())
