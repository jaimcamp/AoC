FILENAME = "everybody_codes_e2024_q01_p1.txt"
dict_values = {"A": 0, "B": 1, "C": 3}


def convert_enemy(line: str) -> int:
    return sum([dict_values[x] for x in line])


def main() -> int:
    with open(FILENAME, "r") as f:
        line = f.readline().strip()
        value_line = convert_enemy(line)
        return value_line


if __name__ == "__main__":
    print(main())
