FILENAME = "everybody_codes_e2024_q01_p3.txt"
# FILENAME = "test_p3.txt"
dict_values = {"A": 0, "B": 1, "C": 3, "D": 5}
potions = [0, 0, 2, 6]


def convert_enemy(line: str) -> int:
    groups = [line[x : x + 3] for x in range(0, len(line), 3)]
    total = 0
    for group in groups:
        subtotal = [dict_values[element] for element in group if element != "x"]
        total += sum(subtotal) + potions[len(subtotal)]
        print(total, group, subtotal, potions[len(subtotal)])
    return total


def main() -> int:
    with open(FILENAME, "r") as f:
        line = f.readline().strip()
        value_line = convert_enemy(line)
        return value_line


if __name__ == "__main__":
    print(main())
