with base as (
select
	*,
	lead(timestamp) over(
order by
	timestamp asc) as l_ts
from
	areas
join sleigh_locations sl 
on
	st_intersects(areas.polygon,
	sl.coordinate)
order by
	timestamp asc)
select
	place_name,
	sum(l_ts - timestamp) as total_ts
from
	base
group by
	place_name
order by
	total_ts desc
