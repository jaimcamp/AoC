select
  split_part(emails, '@', 2) as domain,
  count(*) as cc
from
  (
    select
      explode(split(trim("{}", email_addresses), "[ ,]")) as emails
    from
      contact_list
  )
group by
  domain
order by
  cc desc;
