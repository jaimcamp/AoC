WITH recursive middle (indent, staff_id, staff_name, manager_id, path) as (
        select
            '' as indent,
            staff_id,
            staff_name,
            manager_id,
            array_construct(staff_id) as path
        from
            staff
        where
            manager_id is null
        union all
        select
            indent || '--',
            staff.staff_id,
            staff.staff_name,
            staff.manager_id,
            array_append(middle.path, staff.staff_id) as path
        from
            staff
            join middle on staff.manager_id = middle.staff_id
    )
select
    *,
    array_size(path) as level
from
    middle
order by
    level desc
