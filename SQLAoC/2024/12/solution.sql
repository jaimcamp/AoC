with base as (
  select
    gift_id,
    count(request_id) as total_request
  from
    hive_metastore.default.gift_requests
  group by
    gift_id
)
select
  gift_id,
  gift_name,
  total_request,
  round(
    percent_rank(total_request) OVER (
      ORDER BY
        total_request
    ),
    2
  ) as rank
from
  base
  join gifts using (gift_id)
order by
  rank desc,
  gift_name asc;
