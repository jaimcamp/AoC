WITH base as (
    SELECT
        reindeer_id,
        exercise_name,
        avg(speed_record) as avg_speed
    FROM
        SANTA_WORKSHOP.PUBLIC.TRAINING_SESSIONS
    where
        reindeer_id != 9
    group by
        reindeer_id,
        exercise_name 
    QUALIFY MAX(avg_speed) OVER(partition by reindeer_id) = avg_speed
    order by
        avg_speed desc
)
select
    reindeer_name,
    round(avg_speed, 2)
from
    base
    join reindeers using (reindeer_id);
