with wishes as (
    select
        *
    from
        wish_lists -- where child_id = 515
        qualify row_number() over (
            partition by child_id
            order by
                submitted_date desc
        ) = 1
)
select
    -- *,
    children.name,
    wishes:first_choice::TEXT as primary_wish,
    wishes:second_choice::TEXT as backup_wish,
    wishes:colors [0]::TEXT as favorite_color,
    ARRAY_SIZE(wishes:colors) as color_count,
    CASE
        when difficulty_to_make = 1 then 'Simple Gift'
        when difficulty_to_make = 2 then 'Moderate Gift'
        else 'Complex Gift'
    end as gift_complexity,
    CASE
        when category = 'outdoor' then 'Outside Workshop'
        when category = 'educational' then 'Learning Workshop'
        else 'General Workshop'
    end as workshop_assignment
from
    children
    left join wish_lists using (child_id)
    left join toy_catalogue on primary_wish = toy_name
order by
    children.name asc;
-- lateral flatten ;
-- Tests
select
    *
from
    wish_lists
where
    child_id = 794;
select
    *
from
    children
where
    child_id = 794;
