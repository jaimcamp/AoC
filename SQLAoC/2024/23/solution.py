import polars as pl

uri = "postgresql://postgres:postgres@127.0.0.1/santa_workshop"
query = "SELECT * FROM sequence_table"

table = pl.read_database_uri(query=query, uri=uri).sort("id")

out = (
    table.with_columns(shift_one=pl.col("id").shift(n=1) + 1, diff=pl.col("id").diff(1))
    .filter(pl.col("diff") > 1)
    .with_columns(range=pl.int_ranges("shift_one", "id"))
)


out.select(pl.col("range")).glimpse()
