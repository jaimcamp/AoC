select
    *
from
    (
        select
            * exclude drink_id
        from
            drinks
    ) pivot(
        sum(quantity) for drink_name in (
            any
            order by
                drink_name
        )
    )
where
    "'Hot Cocoa'" = 38
    and "'Peppermint Schnapps'" = 298;
