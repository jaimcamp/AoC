with base as (
    select
        *,
        max(years_experience) over(partition by primary_skill) as max_year_skill,
        min(years_experience) over(partition by primary_skill) as min_year_skill
    from
        workshop_elves qualify years_experience = max_year_skill
        OR years_experience = min_year_skill
    order by
        primary_skill asc,
        years_experience asc
),
pairs as (
    select
        min_group.elf_id as min_elf_id,
        min_group.elf_name as min_elf_name,
        min_group.years_experience as min_years_experience,
        max_group.elf_id as max_elf_id,
        max_group.elf_name as max_elf_name,
        max_group.years_experience as max_years_experience,
        min_group.primary_skill as primary_skill,
    from
        base as min_group
        join base as max_group on min_group.primary_skill = max_group.primary_skill
        and min_group.years_experience = min_group.min_year_skill
        and max_group.years_experience = max_group.max_year_skill
    order by
        primary_skill,
        min_elf_id,
        max_elf_id
)
select
    LEAST(min_elf_id, max_elf_id) as id_1,
    GREATEST(min_elf_id, max_elf_id) as id_2,
    primary_skill -- *
from
    pairs 
    qualify row_number() over(
        partition by primary_skill
        order by
            min_elf_id,
            max_elf_id
    ) = 1
order by
    primary_skill,
    id_1,
    id_2;
