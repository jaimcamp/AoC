select
    *,
    LAG(toys_produced, 1) over (
        order by
            production_date asc
    ) as previous_day_production,
    toys_produced - previous_day_production as production_change,
    production_change / toys_produced * 100 as production_change_percentage
from
    toy_production
order by
    production_change_percentage desc nulls last;
