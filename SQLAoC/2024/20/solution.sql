load postgres
;

attach 'dbname=santa_workshop user=postgres host=127.0.0.1 password=postgres' as db(
    type postgres, read_only
)
;

select
    url,
    str_split(split_part(url, '?', 2), '&') as parameters_array,
    list_unique(
        list_transform(parameters_array, x -> split_part(x, '=', 1))
    ) as l_params
from db.public.web_requests
where parameters_array && ['utm_source=advent-of-sql']
order by l_params desc, url asc
;
