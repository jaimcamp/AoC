-- create table CHRISTMAS_MENUS_XML as
-- select
--     id,
--     parse_xml(MENU_DATA) as MENU_DATA
-- from
--     CHRISTMAS_MENUS;
WITH base as (
    select
        menu_data:"$" as data,
        menu_data:"@version" as version,
    from
        christmas_menus_xml
    where
        version < 3
),
v_2 as (
    select
        -- data,
        XMLGET(
            XMLGET(data, 'attendance_record'),
            'total_guests'
        ):"$" as total_guests,
        XMLGET(
            XMLGET(data, 'menu_registry'),
            'course_details'
        ) as course_details,
        XMLGET(courses.value, 'food_item_id'):"$"::integer as food_id
    from
        base,
        lateral flatten(course_details:"$") as courses
    where
        version = 2
        and total_guests > 78
),
v_1 as (
    select
        -- data,
        XMLGET(
            XMLGET(
                XMLGET(XMLGET(data, 'event_metadata'), 'dinner_details'),
                'guest_registry'
            ),
            'total_count'
        ):"$"::integer as total_guests,
        XMLGET(XMLGET(data, 'event_metadata'), 'menu_items') as menu_items,
        XMLGET(courses_2.value, 'food_item_id'):"$"::integer as food_id
    from
        base,
        lateral flatten(menu_items:"$") as courses,
        lateral flatten(courses.VALUE:"$") as courses_2,
    where
        version = 1
        and total_guests > 78
),
union_vs as (
    select
        food_id
    from
        v_1
    union all
    select
        food_id
    from
        v_2
)
select
    food_id,
    count(1) as cc
from
    union_vs
group by
    food_id
order by
    cc desc;
