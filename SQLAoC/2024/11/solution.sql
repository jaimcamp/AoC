select
    *,
    case
        when season = 'Spring' then 1
        when season = 'Summer' then 2
        when season = 'Fall' then 3
        when season = 'Winter' then 4
    end as season_int,
    AVG(trees_harvested) OVER(
        PARTITION BY field_name
        ORDER BY
            harvest_year,
            season_int ROWS BETWEEN 2 PRECEDING
            and CURRENT ROW
    ) as moving_ave
from
    treeharvests
order by
    -- field_name, harvest_year asc, season_int asc
    moving_ave desc
