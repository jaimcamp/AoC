select
    children.child_id,
    children.name,
    price,
    gifts.name
from
    children
    join gifts on children.child_id = gifts.child_id
    and gifts.price > (
        select
            AVG(price)
        from
            gifts
    )
order by
    price asc
