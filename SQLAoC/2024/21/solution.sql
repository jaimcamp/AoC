load postgres
;

attach 'dbname=santa_workshop user=postgres host=127.0.0.1 password=postgres' as db(
    type postgres, read_only
)
;

select
    year(sale_date) || ',' || quarter(sale_date) as year_q,
    sum(amount) as total_quarter,
    lag(total_quarter) over (order by year_q) as prev_quarter,
    (total_quarter - prev_quarter) / prev_quarter as q_change
from db.public.sales
group by year_q
order by q_change desc
;
