with union_letters as (
    select
        *,
        'A' as source
    from
        letters_a
    union all
    select
        *,
        'B' as source
    from
        letters_b
)
select
    LISTAGG(CHAR(VALUE))
    
from
    union_letters
where
    regexp_like(char(value), '[a-zA-Z ,!?]')
order by
    source,
    id;
