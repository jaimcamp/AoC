-- create table toy_production_array as
-- select
--     toy_id,
--     toy_name,
--     split(
--         replace(replace(previous_tags, '{', ''), '}', ''),
--         ','
--     ) as previous_tags,
--     split(
--         replace(replace(new_tags, '{', ''), '}', ''),
--         ','
--     ) as new_tags
-- from
--     toy_production;
----------
select
    *,
    array_except(new_tags, previous_tags) as added_tags,
    ARRAY_INTERSECTION(new_tags, previous_tags) as unchanged_tags,
    array_except(previous_tags, new_tags) as removed_tags,
    ARRAY_SIZE(added_tags) as added_tags_l,
    ARRAY_SIZE(unchanged_tags) as unchanged_tags_l,
    ARRAY_SIZE(removed_tags) as removed_tags_l
from
    toy_production_array
order by
    added_tags_l desc;
