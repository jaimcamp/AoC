import polars as pl

uri = "postgresql://postgres:postgres@127.0.0.1/santa_workshop"
users = pl.read_database_uri(query="select * from users;", uri=uri)
songs = pl.read_database_uri(query="select * from songs;", uri=uri)
user_plays = pl.read_database_uri(query="select * from user_plays;", uri=uri)

owt = (
    user_plays.join(songs, on="song_id", how="inner")
    .group_by(["song_id", "song_title"])
    .agg(
        pl.len().alias("total"),
        (pl.col("song_duration") > pl.col("duration")).count().alias("skips"),
    )
).sort(by=["total", "skips"], descending=[True, False])

owt.head(1).select(pl.col("song_title")).glimpse()
