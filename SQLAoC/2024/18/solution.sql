with recursive middle (indent,
staff_id,
staff_name,
manager_id,
path) as (
select
	'' as indent,
	staff_id,
	staff_name,
	manager_id,
	array[staff_id] as path
from
	staff
where
	manager_id is null
union all
select
	middle.indent || '--',
	staff.staff_id,
	staff.staff_name,
	staff.manager_id,
	array_append(middle.path,
	staff.staff_id) as path
from
	staff
join middle on
	staff.manager_id = middle.staff_id
    ),
    add_level as (
select
	*,
	array_length(path,
	1) as level
from
	middle
	)
	
	select
	*,
	count(*) over (partition by manager_id) as peers_same_manager,
	
    count(*) over (partition by level) as total_peers_same_level
from
	add_level
order by
	total_peers_same_level desc,
	level asc,
	staff_id asc
