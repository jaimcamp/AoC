with join_kidss as (
select
	children.*
from
	children
inner join christmaslist on
	children.child_id = christmaslist.child_id 
)
,
	 average_kidss as (
select
	city,
	country,
	AVG(naughty_nice_score) as avg_score,
	COUNT(child_id) as cc
from
	join_kidss
group by
	city,
	country
order by
	avg_score desc
	)
select
	*
from
	average_kidss
	where cc >=5;
