import { readFileSync } from 'fs';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n")

let bin_input = input.map(x => x.replace(/F|L/g, '0')).map(x => x.replace(/B|R/g,'1'))

let ids: number[] = [];

let position_fb = function(pos: string[]): number {
    let total:number = 0;
    total += +pos[0] * 64;
    total += +pos[1] * 32;
    total += +pos[2] * 16;
    total += +pos[3] * 8;
    total += +pos[4] * 4;
    total += +pos[5] * 2;
    total += +pos[6] * 1;
    return total
}
let position_lr = function(pos: string[]): number {
    let total:number = 0;
    total += +pos[0] * 4;
    total += +pos[1] * 2;
    total += +pos[2] * 1;
    return total
}

for (let passport of bin_input) {
    let rows = passport.slice(0, 7).split('')
    let cols = passport.slice(-3).split('')
    let row_pos = position_fb(rows)
    let col_pos = position_lr(cols)
    let id = (row_pos * 8) + col_pos
    ids.push(id);
}

console.log(
    ids.reduce((a,b) => Math.max(a,b))
)
//console.log(
    //position_fb('BBFFBBFRLL'.replace(/F|L/g, '0').replace(/B|R/g,'1').slice(0,7).split('')) * 8 +
    //position_lr('BBFFBBFRLL'.replace(/F|L/g, '0').replace(/B|R/g,'1').slice(-3).split(''))
//)

