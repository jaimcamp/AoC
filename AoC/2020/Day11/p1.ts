import { readFileSync } from 'fs';
import { countBy, keys, values } from 'underscore';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n")

const seats_taken = function(current_karte: string[][]) {
    let out = Array.from(Array(current_karte.length), _ => Array(current_karte[0].length).fill(0));
    let tmp: string[][] = [[]];
    tmp[0] = new Array<string>(current_karte.length + 2).fill('.')
    for (let i = 1; i < current_karte.length + 1; i++) {
        tmp[i] = ["."].concat(current_karte[i - 1]).concat(["."])
    }
    tmp[current_karte.length + 1] = new Array<string>(current_karte.length + 2).fill('.')

    for (let i = 1; i < tmp.length - 1; i++) {
        for (let j = 1; j < tmp[0].length - 1; j++) {
            let total =
                (tmp[i - 1][j - 1] == '#' ? 1 : 0) +
                (tmp[i - 1][j + 1] == '#' ? 1 : 0) +
                (tmp[i - 1][j] == '#' ? 1 : 0) +
                (tmp[i][j - 1] == '#' ? 1 : 0) +
                (tmp[i][j + 1] == '#' ? 1 : 0) +
                (tmp[i + 1][j - 1] == '#' ? 1 : 0) +
                (tmp[i + 1][j + 1] == '#' ? 1 : 0) +
                (tmp[i + 1][j] == '#' ? 1 : 0)
            out[i - 1][j - 1] = total;
        }
    }
    return out;
}

class Seats {
    karte: string[][] = [[]];
    old_karte: string[][];
    step_number: number = 0;

    constructor(input_karte: string[]) {
        let index = 0;
        for (let line of input_karte) {
            this.karte[index] = line.split('');
            index++
        }
    }

    step() {
        this.old_karte = JSON.parse(JSON.stringify(this.karte));
        let occupieds = seats_taken(JSON.parse(JSON.stringify(this.karte)));
        this.step_number++;
        for (let i = 0; i < this.karte.length; i++) {
            for (let j = 0; j < this.karte[0].length; j++) {
                let n_occ = occupieds[i][j];
                if (this.karte[i][j] == '#' && n_occ >= 4)
                    this.karte[i][j] = 'L';
                if (this.karte[i][j] == 'L' && n_occ == 0)
                    this.karte[i][j] = '#';
            }
        }
    }

    difference(): number {
        let out = 0;
        for (let i = 0; i < this.karte.length; i++) {
            for (let j = 0; j < this.karte[0].length; j++) {
                out += this.karte[i][j] != this.old_karte[i][j] ? 1 : 0;
            }
        }
        return out;
    }

    get_occupied(): number {
        let out = 0;
        for (let i = 0; i < this.karte.length; i++) {
            for (let j = 0; j < this.karte[0].length; j++) {
                out += this.karte[i][j] == '#' ? 1 : 0;
            }
        }
        return out;

    }
}

let a = new Seats(input);
let diff = -1;
while (true) {
    if (diff == 0) {
        console.log('DONE', a.step_number, a.karte, a.get_occupied())
        break;
    }
    a.step();
    diff = a.difference();
    console.log(diff)
}
