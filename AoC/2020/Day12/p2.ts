import { readFileSync } from 'fs';

const input = readFileSync('ex1.txt', 'utf-8').toString().trim().split("\n")
//console.log(input)
type Move = {
    movement: string;
    amount: number;
};

let moves: Move[] = [];

for (let line of input) {
    let move: Move = {
        'movement': line[0],
        'amount': +line.substr(1, line.length)
    }
    moves.push(move)
}

let anges_pos: { [key: number]: string } = {
    0: 'N',
    90: 'E',
    180: 'S',
    270: 'W',
    360: 'N',
}

class Ferry {
    x: number;
    y: number;
    position: number[];
    orientation: string;
    grados: number;

    constructor() {
        this.x = 0;
        this.y = 0;
        this.position = [this.x, this.y]
        this.orientation = 'E';
        this.grados = 90;
    }

    move(mov: Move) {
        if (mov.movement == 'N') {
            this.y += mov.amount;
        } else if (mov.movement == 'S') {
            this.y -= mov.amount;
        } else if (mov.movement == 'E') {
            this.x += mov.amount;
        } else if (mov.movement == 'W') {
            this.x -= mov.amount;
        } else if (mov.movement == 'L') {
            let g = this.grados - mov.amount
            g = g < 0 ? g + 360 : g;
            this.grados = g >= 360 ? g - 360 : g;
            this.orientation = anges_pos[this.grados]
        } else if (mov.movement == 'R') {
            let g = this.grados + mov.amount
            g = g < 0 ? g + 360 : g;
            this.grados = g >= 360 ? g - 360 : g;
            this.orientation = anges_pos[this.grados]
        } else if (mov.movement == 'F') {
            if (this.orientation == 'N')
                this.y += mov.amount;
            if (this.orientation == 'S')
                this.y -= mov.amount;
            if (this.orientation == 'E')
                this.x += mov.amount;
            if (this.orientation == 'W')
                this.x -= mov.amount;
        }
    }
}

let ferry = new Ferry();
let waypoint = new Ferry();
console.log(ferry.position)

for (let move of moves) {
    if (['N', 'S', 'E', 'W'.includes(move.movement)]){
        waypoint.move(move)
    } else if ( move.movement == 'R'){
    }
    ferry.move(move)
    console.log(ferry.y, ferry.x, ferry.orientation, ferry.grados)
}
console.log(Math.abs(ferry.x) + Math.abs(ferry.y))
