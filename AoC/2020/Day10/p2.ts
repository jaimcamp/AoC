import { readFileSync } from 'fs';
import _ = require('underscore');

let input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n").map(Number)
input = [0].concat(input).concat(Math.max(...input)+3);
let total:number[] = new Array<number>(input.length).fill(0)
total[0] = 1;

input = input.sort((a, b) => a-b)

for (let i = 1; i < input.length; i++){
    for (let j = 0; j < i; j++){
        if ( [1, 2, 3].includes(input[i] - input[j]) ){
            total[i] += total[j]
        }
    }
}
console.log(total.slice(-1)[0])
