import { readFileSync } from 'fs';
import _ from 'underscore';

let input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n").map(Number)

let position = 0;
let used: number[] = [];
let logs: number[] = [];
let possible_pos: number[];

console.log(input);

while (input.length > 0) {
    possible_pos = [1, 2, 3].map(x => position + x)
    for (let pos of possible_pos) {
        if (input.includes(pos)){
            const ix = input.indexOf(pos);
            input.splice(ix, 1);
            used.push(pos);
            logs.push(pos - position);
            position = pos;
            break;
        }
    }
}
//extra
logs.push(3);
console.log(used, _.countBy(logs));
