import { readFileSync } from 'fs';

const array = readFileSync('input.txt', 'utf-8').toString().split("\n").filter(Boolean);

type Rule = {
    letter: string;
    min: number;
    max: number;
};

let passwords: { [password: string]: Rule } = {};

for (let line of array) {
    let _tmp = line.split(':');
    let pass = _tmp[1].trim();
    let _tmp2 = _tmp[0].trim().split(' ');
    let letter = _tmp2[1]
    let [min, max] = _tmp2[0].split('-').map(Number)

    passwords[pass] = {
        letter: letter,
        min: min,
        max: max,

    }
}

let total: number = 0;

for (let key in passwords) {
    let value = passwords[key];
    if ((key[value.min - 1] == value.letter) != (key[value.max - 1] == value.letter)) {
        total += 1;
        console.log(key)
        console.log(key[value.min - 1], value.min)
    }
}

console.log(total)
