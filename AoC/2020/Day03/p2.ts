import { readFileSync } from 'fs';

const karte = readFileSync('input.txt', 'utf-8')
    .toString().split("\n").filter(Boolean).map(x => x.split(''));
let bigkarte = karte;

let trees: number[] = [0, 0, 0, 0, 0];
let slopes: number[][] =
    [[1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2]];

let i: number = 0;
let j: number = 0;

let max_i = karte[0].length - 1;
let max_j = karte.length - 1;

let move = function(x: number, y: number, x_amount: number, y_amount: number): number[] {
    return [x + x_amount, y + y_amount];
};

let grow = function(bigarray: string[][]): string[][] {
    let tmp = bigarray.map(x => x.concat(x))
    return tmp;
};

for (let index of Array.from(Array(5).keys())) {
    let slope = slopes[index];
    let tree = trees[index];
    console.log(slope, tree)

    while (j <= max_j) {
        if (bigkarte[j][i] == '#') {
            tree += 1;
        }

        let [_i, _j] = move(i, j, slope[0], slope[1]);
        i = _i;
        j = _j;
        if (i >= max_i) {
            bigkarte = grow(bigkarte)
            max_i = bigkarte[0].length - 1;
        }
    }
    trees[index] = tree
    i = j = 0;
};
console.log(trees);
console.log(trees.reduce((a, b) => a * b, 1));

