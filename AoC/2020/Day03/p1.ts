import { readFileSync } from 'fs';

const karte = readFileSync('input.txt', 'utf-8')
    .toString().split("\n").filter(Boolean).map(x => x.split(''));
let bigkarte = karte;

console.log(karte[0]);

let trees: number = 0;

let i: number = 0;
let j: number = 0;

let max_i = karte[0].length - 1;
let max_j = karte.length - 1;

let move = function(x: number, y:number): number[] {
    return [x + 3, y + 1];
};

let grow = function(bigarray: string[][]): string[][] {
    let tmp = bigarray.map(x => x.concat(x))
    return tmp;
};

while (j <= max_j) {
    console.log(i, j);
    console.log(bigkarte[j][i]);
    if (bigkarte[j][i] == '#'){
        trees += 1;
    }

    let [_i, _j] = move(i, j);
    i = _i;
    j = _j;
    if (i >= max_i){
        bigkarte = grow(bigkarte)
        max_i = bigkarte[0].length - 1;
        console.log('new dim', max_i, max_j)
    }
}
console.log(trees);

