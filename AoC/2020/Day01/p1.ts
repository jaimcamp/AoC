import { writeFileSync, readFileSync } from 'fs';

var array = readFileSync('input.txt', 'utf-8').toString().split("\n").filter(Boolean).map(Number);

array.forEach((value, index) => {
    const idx = array.slice(index + 1).map(x => x + value).indexOf(2020)
    if (idx > 0) {
        console.log(value);
        console.log(array.slice(index + 1)[idx]);
        console.log(value * array.slice(index + 1)[idx]);
        return;
    }

})
