import { readFileSync } from 'fs';
import { countBy, keys, values } from 'underscore';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n\n")

let together = input.map(x => x.replace(/\n/g, ''))
let peoples = input.map(x => x.split('\n').length)

let total = 0;

for (let i = 0; i < together.length; i++) {
    let _tmp = values(countBy(together[i]))
        .map(x => x == peoples[i] ? 1 : 0)
        .reduce((a, b) => a + b, 0)

    total += _tmp
}

console.log(total)
