import { readFileSync } from 'fs';
import { countBy, keys, values} from 'underscore';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n\n")

let together = input.map(x => x.replace(/\n/g, ''))

let total = 0;

for (let group of together) {
    let _tmp = Object.keys(countBy(group)).length
    total += _tmp
}

console.log(total)
