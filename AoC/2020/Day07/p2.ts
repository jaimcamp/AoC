import * as fs from 'fs'
import * as path from 'path'

const input = fs.readFileSync(path.join(__dirname, 'input.txt'), 'utf-8').toString().trim().split("\n").slice(0, 4)

interface ContentBag {
    color: string;
    amount: number;
}

console.log(input)

const colourToContent = input.reduce((map, line) => {
    const [colour, contentsString] = line.split(" bags contain ");
    const contents = contentsString.split(", ").map((quantityColor) => {
        const matches = quantityColor.match(/(\d*) (.*) bag[s\.]*/);
        if (!matches) {
            throw new Error("Regex failed");
        }
        const colour = matches[2].replace(".", "");
        const count = colour === "other" ? 0 : parseInt(matches[1], 10);
        return { colour, count };
    });
    return map.set(colour, contents);
}, new Map<string, ContentBag[]>());
// potential memoize opportunity
//function canContainColour(bagColour: string, containedColor: string): boolean {
    //const contents = colourToContent.get(bagColour) || [];
    //if (contents.some((content) => content.colour === containedColor)) {
        //return true;
    //}
    //return contents.some((content) =>
        //canContainColour(content.colour, containedColor)
    //);
//}
//// potential memoize opportunity
//function countBags(colour: string): number {
//const contents = colourToContent.get(colour) || [];
    //// +1 for the container bag
    //return (
        //1 +
            //contents
            //.map((content) => content.count * countBags(content.colour))
            //.reduce((total, current) => total + current, 0)
    //);
//}
//// part 1
//const canContainBags = [...colourToContent.keys()].filter((bagColour) =>
    //canContainColour(bagColour, "shiny gold")
//);
//console.log(canContainBags.length);
//// part 2 - subtract 1 as we don't count our shiny gold bag
//const neededBags = countBags("shiny gold") - 1;
//console.log(neededBags);
