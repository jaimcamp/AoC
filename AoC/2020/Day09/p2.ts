import { readFileSync } from 'fs';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n").map(Number);

console.log(input);

const combinations = function(array: number[], t: number) {

    let acc = 0;
    let output = [];
    for (let x of array) {
        acc += x;
        output.push(x);
        if (acc > t)
            return [];
        if (acc == t)
            return output;
    }
}

const target: number = 27911108;

for (let i = 0; i < input.length; i++) {
    let base = input.slice(i);
    let _tmp = combinations(base, target);
    if (_tmp.length == 0)
        continue;
    else {
        let min = Math.min(..._tmp);
        let max = Math.max(..._tmp);
        console.log('Min', min, 'Max', max, 'Total', min + max);
    }
}
