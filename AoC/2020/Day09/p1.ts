import { readFileSync } from 'fs';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n").map(Number);

console.log(input);

const combinations = function(array: number[]) {

    let out = [];
    for (let i = 0; i < array.length - 1; i++) {
        for (let j = i + 1; j < array.length; j++) {
            out.push(array[i] + array[j]);
        }
    }
    return out;
}

const start_at:number = 25;
const amount:number = 25;
let founded_idxs = [];

for (let i = start_at; i < input.length; i++){
    let base = input.slice(i - amount, i);
    let value = input[i];
    let _tmp = combinations(base).findIndex(x => x == value);
    founded_idxs.push(_tmp);
    if (_tmp < 0)
        console.log('Bad value', value, 'idx', i);
}
console.log(founded_idxs);
