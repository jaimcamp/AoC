import { readFileSync } from 'fs';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n\n")
    .map(x => x.replace(/\n/g, ' '))

let check_h = function(height: string): boolean {
    let mess = height.substr(-2);
    let value = +height.substr(0, height.length - 2);
    if (mess == 'cm') {
        return value >= 150 && value <= 193;
    }
    if (mess == 'in') {
        return value >= 59 && value <= 76;
    }
    else {
        return false;
    }
}

class Passport {
    byr: number;
    iyr: number;
    eyr: number;
    hgt: string;
    hcl: string;
    ecl: string;
    pid: string;
    cid: string;

    constructor(values: string[][]) {
        for (let pair of values) {
            if (pair[0] == 'byr' || pair[0] == 'iyr' || pair[0] == 'eyr') {
                this[pair[0]] = +pair[1];
            }
            else {
                this[pair[0]] = pair[1];
            }
        }
    }

    isValid(): number {
        let existing =
            'byr' in this &&
            'iyr' in this &&
            'eyr' in this &&
            'hgt' in this &&
            'hcl' in this &&
            'ecl' in this &&
            'pid' in this
        if (!existing) {
            return 0
        }
        let v_byr = this.byr >= 1920 && this.byr <= 2002;
        let v_iyr = this.iyr >= 2010 && this.iyr <= 2020;
        let v_eyr = this.eyr >= 2020 && this.eyr <= 2030;
        let v_hgt = check_h(this.hgt);
        let v_hcl = this.hcl.match('^#[abcdef0123456789]{6}') != null
        let v_ecl =
            this.ecl == 'amb' || this.ecl == 'blu' ||
            this.ecl == 'brn' || this.ecl == 'gry' || this.ecl == 'grn' ||
            this.ecl == 'hzl' || this.ecl == 'oth';
        let v_pid = this.pid.match('^[0-9]{9}$') != null
        let out =
            v_byr &&
            v_iyr &&
            v_eyr &&
            v_hgt &&
            v_hcl &&
            v_ecl &&
            v_pid;
        return out ? 1 : 0
    }
};

let passports: Passport[] = []

for (let line of input) {
    let line_elements = line.split(' ').map(x => x.split(':'));
    let _pass = new Passport(line_elements);
    passports.push(_pass);
}
console.log(passports[0], passports[0].isValid());
console.log(passports.map(x => x.isValid()).reduce((sum, current) => sum + current))
