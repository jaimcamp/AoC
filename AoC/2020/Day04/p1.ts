import { readFileSync } from 'fs';

const input = readFileSync('input.txt', 'utf-8').toString().trim().split("\n\n")
    .map(x => x.replace(/\n/g, ' '))

class Passport {
    byr: string;
    iyr: string;
    eyr: string;
    hgt: string;
    hcl: string;
    ecl: string;
    pid: string;
    cid: string;

    constructor(values: string[][]) {
        for (let pair of values) {
            this[pair[0]] = pair[1];
        }
    }

    isValid(): number {
        let out = 'byr' in this &&
            'iyr' in this &&
            'eyr' in this &&
            'hgt' in this &&
            'hcl' in this &&
            'ecl' in this &&
            'pid' in this
        return out ? 1 : 0
    }
};

let passports: Passport[] = []

for (let line of input) {
    let line_elements = line.split(' ').map(x => x.split(':'));
    let _pass = new Passport(line_elements);
    passports.push(_pass);
}
console.log(passports.map(x => x.isValid()).reduce((sum, current) => sum + current))
