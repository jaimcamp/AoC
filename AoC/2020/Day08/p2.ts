import { readFileSync } from 'fs';
var _ = require('underscore');

const input = readFileSync('input.txt', 'utf-8').toString().trim()
    .split("\n").map(x => x.split(' '));

console.log(input);

let indexes = input.reduce(function(a, e, i) {
    if (e[0] == 'nop' || e[0] == 'jmp')
        a.push(i);
    return a;
}, new Array<number>())


let change = { 'nop': 'jmp', 'jmp': 'nop' };
let accumulator: number

//let firstrun: boolean
let index: number
let seen: number[]

for (let ix of indexes) {
    let new_input = JSON.parse(JSON.stringify(input));
    let tmp = new_input[ix][0];
    new_input[ix][0] = change[tmp];
    accumulator = 0;
    //firstrun = true;
    index = 0;
    seen = [];
    console.log('Start change', ix);
    while (true) {
        if (seen.includes(index)) {
            //console.log('total', accumulator, 'index', index);
            break;
        }
        if (index >= 659 ) {
            //console.log('total', accumulator, 'index', index);
            break;
        }

        seen.push(index);
        let inst: string = new_input[index][0];
        let value: number = +new_input[index][1];

        //console.log(index, inst, value);
        if (inst == 'acc') {
            accumulator += value;
            index++;
            continue;

        } else if (inst == 'nop') {
            index++;
            continue;
        } else if (inst == 'jmp') {
            index += value;
            continue;

        }
    }
    if (index >= 600) console.log('Finished', accumulator, index)
}

