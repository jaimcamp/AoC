import { readFileSync } from 'fs';

const input = readFileSync('input.txt', 'utf-8').toString().trim()
    .split("\n").map(x => x.split(' '));

console.log(input);

let accumulator: number = 0;

let firstrun: boolean = true;
let index: number = 0;
let seen: number[] = [];
while (firstrun) {
    console.log(index);
    if (seen.includes(index)){
        console.log('total', accumulator);
        break;
    }

    seen.push(index);
    let inst: string = input[index][0];
    let value: number = +input[index][1];

    if (inst == 'acc') {
        accumulator += value;
        index++;
        continue;

    } else if (inst == 'nop') {
        index++;
        continue;
    } else if (inst == 'jmp') {
        index += value;
        continue;

    }
}
