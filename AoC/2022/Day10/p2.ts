import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals } from '../deps.ts';

function p2(input_text: string): void {
	const instructions = input_text.trim().split('\n');
	let X: number[] = [1];
	const screen = [...Array(6)].map((_) => Array(40).fill('') as string[]);
	for (let index = 0; index < instructions.length; index++) {
		const instruction = instructions[index];
		const last_val = X[X.length - 1];
		switch (instruction.slice(0, 1)) {
			case 'n':
				X.push(last_val);
				break;
			case 'a': {
				const [_, amount] = instruction.split(' ');
				const new_amount = last_val + Number(amount);
				X = X.concat([last_val, new_amount]);
				break;
			}

			default:
				break;
		}
	}
	let cycle = 1;
	for (let j = 0; j < screen.length; j++) {
		for (let i = 0; i < screen[0].length; i++) {
			const position = (j * 40) + i;
			if (position % 40 >= X[position] - 1 && position % 40 <= X[position] + 1) {
				screen[j][i] = '#';
			}
			/* console.log(position, screen[j][i], X[position]); */
			cycle++;
		}
	}

	console.table(screen);
}

/* Deno.test('Test p1 example', () => { */
/* 	const output_p1 = p1(example); */
/* 	assertEquals(example_p1, output_p1); */
/* }); */
Deno.test('Test p2 example', () => {
	p2(example);
	/* assertEquals(example_p2, output_p2); */
});

/* console.log('Real Imput, part 1', p1(input)); */
console.log('Real Imput, part 2', p2(input));
