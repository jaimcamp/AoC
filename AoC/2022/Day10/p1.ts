import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals } from '../deps.ts';

function p1(input_text: string): number {
	const instructions = input_text.trim().split('\n');
	let signal_strength = 0;
	let X: number[] = [1];
	for (let index = 0; index < instructions.length; index++) {
		const instruction = instructions[index];
		const last_val = X[X.length - 1];
		switch (instruction.slice(0, 1)) {
			case 'n':
				X.push(last_val);
				break;
			case 'a': {
				const [_, amount] = instruction.split(' ');
				const new_amount = last_val + Number(amount);
				X = X.concat([last_val, new_amount]);
				break;
			}

			default:
				break;
		}
	}
	for (let index = 19; index < X.length; index += 40) {
		signal_strength += X[index] * (index + 1);
		console.log(X[index], (index+1));
	}
	console.log(signal_strength);
	return signal_strength;
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});
/* Deno.test('Test p2 example', () => { */
/* 	const output_p2 = p2(example); */
/* 	assertEquals(example_p2, output_p2); */
/* }); */

console.log('Real Imput, part 1', p1(input));
/* console.log('Real Imput, part 2', p2(input)); */
