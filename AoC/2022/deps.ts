export { assert, assertEquals } from 'https://deno.land/std@0.166.0/testing/asserts.ts';
export {
	add,
	cloneDeep,
	intersection,
	isEqual,
	range,
	zip,
} from 'https://raw.githubusercontent.com/lodash/lodash/4.17.21-es/lodash.js';
export { cartesianProduct } from './utils.ts';
