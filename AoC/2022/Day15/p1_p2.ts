import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, cartesianProduct, range, zip } from '../deps.ts';

function manhattan(point_a: number[], point_b: number[]): number {
	return Math.abs(point_b[0] - point_a[0]) +
		Math.abs(point_b[1] - point_a[1]);
}

function consolidate_positions(segments: number[][]): number[][] {
	let out = [];
	while (segments.length) {
		const [l_min, l_max] = segments.pop() as [number, number];

		if (segments.length < 1) {
			out.push([l_min, l_max]);
		} else {
			for (let index = 0; index < segments.length; index++) {
				const element = segments[index];
				if (
					!(element[0] > l_min && element[0] > l_max) &&
					!(element[1] < l_min && element[1] < l_min)
				) {
					element[0] = Math.min(element[0], l_min);
					element[1] = Math.max(element[1], l_max);
					continue;
				} else {
					out.push([l_min, l_max]);
				}
			}
		}
	}
	return out;
}
function get_all_position(
	sensor: [number, number],
	beacon: [number, number],
	segments: number[][],
	beacons: string[],
	y_pos: number,
): void {
	const distance = manhattan(sensor, beacon);
	if (y_pos >= sensor[1] - distance && y_pos <= sensor[1] + distance) {
		const l_min = sensor[0] - (distance - Math.abs(sensor[1] - y_pos));
		const l_max = sensor[0] + (distance - Math.abs(sensor[1] - y_pos));
		if (segments.length < 1) {
			segments.push([l_min, l_max]);
		} else {
			for (let index = 0; index < segments.length; index++) {
				const element = segments[index];
				if (
					!(element[0] > l_min && element[0] > l_max) &&
					!(element[1] < l_min && element[1] < l_min)
				) {
					element[0] = Math.min(element[0], l_min);
					element[1] = Math.max(element[1], l_max);
					continue;
				} else {
					segments.push([l_min, l_max]);
				}
			}
		}
	}
}

function p1(input_text: string, y_pos: number): number {
	let total = 0;
	const _r = /-?\d+/g;
	const segments: number[][] = [];
	const vals = input_text.trim().split('\n').map((line) => line.match(_r)!.map(Number)) as [
		number,
		number,
		number,
		number,
	][];
	const beacons = vals.map((e) => e.slice(2).join());
	for (let index = 0; index < vals.length; index++) {
		const sensor = vals[index].slice(0, 2) as [number, number];
		const beacon = vals[index].slice(2) as [number, number];
		get_all_position(sensor, beacon, segments, beacons, y_pos);
	}
	const unique_segments = consolidate_positions(segments);
	return unique_segments.map((segment) => segment[1] - segment[0]).reduce((a, b) => a + b);
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example, 10);
	assertEquals(example_p1, output_p1);
});

/* Deno.test("Test p2 example", () => { */
/*   const output_p2 = p2(example); */
/*   assertEquals(example_p2, output_p2); */
/* }); */

console.log('Real Imput, part 1', p1(input, 2000000));
/* console.log("Real Imput, part 2", p2(input)); */
