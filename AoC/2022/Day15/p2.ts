import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, cartesianProduct, cloneDeep, range } from '../deps.ts';

function manhattan(point_a: number[], point_b: number[]): number {
	return Math.abs(point_b[0] - point_a[0]) +
		Math.abs(point_b[1] - point_a[1]);
}

function consolidate_positions(segments: number[][]): number[][] {
	if (segments.length === 0) return segments;

	const new_segments: number[][] = [];
	segments.sort((a, b) => a[0] - b[0]);
	let [start, end] = segments[0];

	for (let index = 0; index < segments.length; index++) {
		const [current_segment_start, current_segment_end] = segments[index];
		if (end + 1 < current_segment_start) {
			new_segments.push([start, end]);
			start = current_segment_start;
			end = current_segment_end;
		} else {
			end = Math.max(current_segment_end, end);
		}
	}
	new_segments.push([start, end]);
	return new_segments;
}
function get_all_position(
	sensor: [number, number],
	beacon: [number, number],
	segments: number[][],
	y_pos: number,
): void {
	const distance = manhattan(sensor, beacon);
	if (y_pos >= sensor[1] - distance && y_pos <= sensor[1] + distance) {
		const l_min = sensor[0] - (distance - Math.abs(sensor[1] - y_pos));
		const l_max = sensor[0] + (distance - Math.abs(sensor[1] - y_pos));
		segments.push([l_min, l_max]);
	}
}

function p2(input_text: string, max_val: number): number {
	const _r = /-?\d+/g;
	const vals = input_text.trim().split('\n').map((line) => line.match(_r)!.map(Number)) as [
		number,
		number,
		number,
		number,
	][];
	let out: { 'pos': number; 'segments': number[][] } = { pos: 0, segments: [[0]] };
	for (let y_pos = 0; y_pos <= max_val; y_pos++) {
		const segments: number[][] = [];
		for (let index = 0; index < vals.length; index++) {
			const sensor = vals[index].slice(0, 2) as [number, number];
			const beacon = vals[index].slice(2) as [number, number];
			get_all_position(sensor, beacon, segments, y_pos);
		}
		const unique_segments = consolidate_positions(segments);
		if (unique_segments.length > 1) {
			out = { pos: y_pos, segments: unique_segments };
			break;
		}
		/* console.log(y_pos, segments, unique_segments, unique_segments.length); */
	}
	return (out.pos) + ((out.segments[0][1] + 1) * 4000000);
}

Deno.test('Test p2 example', () => {
	const output_p2 = p2(example, 20);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 2', p2(input, 4000000));
Deno.bench('Part 2 benchmark', () => {
	p2(input, 4000000);
});
