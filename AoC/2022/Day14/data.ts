export const input: string = Deno.readTextFileSync('input.txt');
export const example = `498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9`;

export const example_p1 = 24;
export const example_p2 = 93;
