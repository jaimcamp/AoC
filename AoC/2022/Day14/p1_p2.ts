import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, range } from '../deps.ts';
function create_map(instructions: string[][]): Set<string> {
	const rocks: string[] = [];
	for (let index = 0; index < instructions.length; index++) {
		const path_instruction = instructions[index];
		let [x_start, y_start] = path_instruction[0].split(',').map(Number);
		for (let j = 1; j < path_instruction.length; j++) {
			const [x_end, y_end] = path_instruction[j].split(',').map(Number);
			if (x_end === x_start) {
				const [y_min, y_max] = [y_start, y_end].sort((a, b) => a - b);
				const r = range(y_min, y_max + 1) as number[];
				r.forEach((val) => rocks.push([x_start, val].join()));
			} else {
				const [x_min, x_max] = [x_start, x_end].sort((a, b) => a - b);
				const r = range(x_min, x_max + 1) as number[];
				r.forEach((val) => rocks.push([val, y_start].join()));
			}
			x_start = x_end;
			y_start = y_end;
		}
	}
	return new Set(rocks);
}

function sand_falling(rocks: Set<string>, start: string, y_max: number): string | boolean {
	let is_done = false;
	let out: boolean | string = false;
	let [sand_x, sand_y] = start.split(',').map(Number);
	while (!is_done) {
		if (rocks.has([sand_x, sand_y + 1].join())) {
			if (rocks.has([sand_x - 1, sand_y + 1].join())) {
				if (rocks.has([sand_x + 1, sand_y + 1].join())) {
					rocks.add([sand_x, sand_y].join());
					out = false;
					is_done = true;
				} else {
					sand_x += 1;
					sand_y += 1;
				}
			} else {
				sand_x -= 1;
				sand_y += 1;
			}
		} else {
			sand_y += 1;
		}
		if (sand_y > y_max) {
			out = true;
			is_done = true;
		}
	}
	return out;
}

function apply_sand(rocks: Set<string>, start: string, y_max: number) {
	let keep_pushing = true;
	/* const sand = new Set<string>(); */
	let total_sand = 0;
	while (keep_pushing) {
		const sand_status: boolean | string = sand_falling(rocks, start, y_max);
		if (sand_status) {
			keep_pushing = false;
			continue;
		}
		total_sand++;
	}
	return total_sand;
}

function p1(input_text: string): number {
	const lines = input_text.trim().split('\n').map((line) => line.split(' -> '));
	/* .map((val) => val.split(',')) */
	/* .map((v) => Number(v)))); */
	const start = [500, 0].join();
	/* const max_val = Math.max(...lines.flat(3)); */
	/* const min_val = Math.min(...lines.flat(3)); */
	const y_max = Math.max(
		...lines.flat().map((set_inst) => set_inst.split(',')).map((val) => Number(val[1])),
	);
	const rocks = create_map(lines);
	const total_sand = apply_sand(rocks, start, y_max);

	return total_sand;
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});

/* Deno.test("Test p2 example", () => { */
/*   const output_p2 = p2(example); */
/*   assertEquals(example_p2, output_p2); */
/* }); */

console.log('Real Imput, part 1', p1(input));
/* console.log("Real Imput, part 2", p2(input)); */
