import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, range } from '../deps.ts';
function create_map(
	instructions: string[][],
	x_high: number,
	x_low: number,
	y_high: number,
): Set<string> {
	const rocks: Set<string> = new Set<string>();
	range(x_low - 10000, x_high + 10001).forEach((val: number) =>
		rocks.add([val, y_high + 2].join())
	);
	for (let index = 0; index < instructions.length; index++) {
		const path_instruction = instructions[index];
		let [x_start, y_start] = path_instruction[0].split(',').map(Number);
		for (let j = 1; j < path_instruction.length; j++) {
			const [x_end, y_end] = path_instruction[j].split(',').map(Number);
			if (x_end === x_start) {
				const [y_min, y_max] = [y_start, y_end].sort((a, b) => a - b);
				const r = range(y_min, y_max + 1) as number[];
				r.forEach((val) => rocks.add([x_start, val].join()));
			} else {
				const [x_min, x_max] = [x_start, x_end].sort((a, b) => a - b);
				const r = range(x_min, x_max + 1) as number[];
				r.forEach((val) => rocks.add([val, y_start].join()));
			}
			x_start = x_end;
			y_start = y_end;
		}
	}
	return rocks;
}

function sand_falling(rocks: Set<string>, start: string): boolean {
	let is_done = false;
	let out = false;
	let [sand_x, sand_y] = start.split(',').map(Number);
	while (!is_done) {
		if (rocks.has([500, 0].join())) {
			out = true;
			is_done = true;
		}
		if (rocks.has([sand_x, sand_y + 1].join())) {
			if (rocks.has([sand_x - 1, sand_y + 1].join())) {
				if (rocks.has([sand_x + 1, sand_y + 1].join())) {
					rocks.add([sand_x, sand_y].join());
					is_done = true;
				} else {
					sand_x += 1;
					sand_y += 1;
				}
			} else {
				sand_x -= 1;
				sand_y += 1;
			}
		} else {
			sand_y += 1;
		}
	}
	return out;
}

function apply_sand(rocks: Set<string>, start: string) {
	let keep_pushing = true;
	let total_sand = 0;
	while (keep_pushing) {
		const sand_status: boolean = sand_falling(rocks, start);
		if (sand_status) {
			keep_pushing = false;
			continue;
		}
		total_sand++;
	}
	return total_sand;
}

function p2(input_text: string): number {
	const lines = input_text.trim().split('\n').map((line) => line.split(' -> '));
	const start = [500, 0].join();
	const x_max = Math.max(
		...lines.flat().map((set_inst) => set_inst.split(',')).map((val) => Number(val[0])),
	);
	const x_min = Math.min(
		...lines.flat().map((set_inst) => set_inst.split(',')).map((val) => Number(val[0])),
	);
	const y_max = Math.max(
		...lines.flat().map((set_inst) => set_inst.split(',')).map((val) => Number(val[1])),
	);
	const rocks = create_map(lines, x_max, x_min, y_max);
	const total_sand = apply_sand(rocks, start);

	return total_sand;
}

Deno.test('Test p2 example', () => {
	const output_p2 = p2(example);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 2', p2(input));
