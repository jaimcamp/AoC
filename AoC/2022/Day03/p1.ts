import { example, example_p1, example_p2, input } from "./data.ts";
import { assertEquals, intersection } from "../deps.ts";

const parse = (input_text: string): string[] => {
  return input_text.trim().split("\n");
};

class Rucksack {
  public first_compt_chars: string[];
  public second_compt_chars: string[];
  public sharedElements!: string[];
  constructor(rucksack_text: string) {
    this.first_compt_chars = rucksack_text.slice(0, rucksack_text.length / 2)
      .split("");
    this.second_compt_chars = rucksack_text.slice(
      rucksack_text.length / 2,
      rucksack_text.length,
    ).split("");
  }

  shared_elements() {
    this.sharedElements = intersection(
      this.first_compt_chars,
      this.second_compt_chars,
    );
  }
}

function char_to_int(input_char: string): number {
  const ascii_val = input_char.charCodeAt(0);
  return ascii_val >= 97 ? ascii_val - 96 : ascii_val - 38;
}

function p1(input_text: string) {
  const parse_text = parse(input_text);
  const all_rucksacks: Rucksack[] = [];
  for (let index = 0; index < parse_text.length; index++) {
    const element = parse_text[index];
    const loop_rs = new Rucksack(element);
    loop_rs.shared_elements();
    all_rucksacks.push(loop_rs);
  }
  const intersections_items = all_rucksacks.map((ruck) =>
    ruck.sharedElements.map((val) => char_to_int(val))
  );
  return intersections_items.flat().reduce((a, b) => a + b);
}

Deno.test("Test p1 example", () => {
  const output_p1 = p1(example);
  assertEquals(example_p1, output_p1);
});

console.log("Real Imput, part 1", p1(input));
