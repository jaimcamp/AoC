import { example, example_p1, example_p2, input } from "./data.ts";
import { assertEquals, intersection } from "../deps.ts";

const parse = (input_text: string): string[][] => {
  const n_gnomes = 3;
  const groups = input_text.trim().split("\n");
  const array_groups: string[][] = [];
  for (let index = 0; index < groups.length; index += n_gnomes) {
    const chunk = groups.slice(index, index + n_gnomes);
    array_groups.push(chunk);
  }
  return array_groups;
};

class Rucksack {
  public all_elements: string[][];
  public sharedElements!: string[];
  constructor(rucksack_text: string[]) {
    this.all_elements = rucksack_text.map((line) => line.split(""));
  }

  shared_elements() {
    this.sharedElements = intersection(...this.all_elements);
  }
}

function char_to_int(input_char: string): number {
  const ascii_val = input_char.charCodeAt(0);
  return ascii_val >= 97 ? ascii_val - 96 : ascii_val - 38;
}

function p2(input_text: string) {
  const parse_text = parse(input_text);
  const all_rucksacks: Rucksack[] = [];
  for (let index = 0; index < parse_text.length; index++) {
    const element = parse_text[index];
    const loop_rs = new Rucksack(element);
    loop_rs.shared_elements();
    all_rucksacks.push(loop_rs);
  }
  const intersections_items = all_rucksacks.map((ruck) =>
    ruck.sharedElements.map((val) => char_to_int(val))
  );
  return intersections_items.flat().reduce((a, b) => a + b);
}

Deno.test("Test p2 example", () => {
  const output_p2 = p2(example);
  assertEquals(example_p2, output_p2);
});

console.log("Real Imput, part 2", p2(input));
