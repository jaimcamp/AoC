export const input: string = Deno.readTextFileSync('input.txt');
export const example = `30373
25512
65332
33549
35390`;

export const example_p1 = 21;
export const example_p2 = 4;
