import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, zip } from '../deps.ts';

function parse(input_text: string): number[][] {
	return input_text.trim().split('\n').map((line) => line.split('').map(Number));
}

function can_see_it(i: number, j: number, element: number, array_trees: number[][]) {
	const horizontal_line = array_trees[j];
	const vertical_line = array_trees.map((line) => line[i]);
	const left = horizontal_line.slice(0, i);
	const right = horizontal_line.slice(i + 1);
	const up = vertical_line.slice(0, j);
	const down = vertical_line.slice(j + 1);
	const sides = [left, right, up, down];
	const isBigger = sides.map((side) => side.every((val) => element > val)).some(Boolean);
	return isBigger;
}

function view_quality(i: number, j: number, element: number, array_trees: number[][]) {
	let total_distance = 1;
	const horizontal_line = array_trees[j];
	const vertical_line = array_trees.map((line) => line[i]);
	const left = horizontal_line.slice(0, i).reverse();
	const right = horizontal_line.slice(i + 1);
	const up = vertical_line.slice(0, j).reverse();
	const down = vertical_line.slice(j + 1);
	const sides = [up, left, right, down];
	for (const direction of sides) {
		const index_big_tree = direction.findIndex((val) => val >= element) + 1;
		const dir_view = index_big_tree > 0 ? direction.slice(0, index_big_tree).length : direction.length;
		total_distance *= dir_view;
		/* console.log(index_big_tree, dir_view, total_distance, direction); */
	}

	/* console.log(sides, element, total_distance); */
	return total_distance;
}

function p1(input_text: string) {
	let total_visible = 0;
	const array_trees: number[][] = parse(input_text);
	const width = array_trees[0].length;
	const height = array_trees.length;
	total_visible += (width - 1) * 2;
	total_visible += (height - 1) * 2;
	for (let j = 1; j < array_trees.length - 1; j++) {
		for (let i = 1; i < array_trees[0].length - 1; i++) {
			const element = array_trees[j][i];
			const isSeen = can_see_it(i, j, element, array_trees);
			if (isSeen) {
				total_visible++;
			}
		}
	}
	return total_visible;
}
function p2(input_text: string) {
	let max_visibility = 0;
	const array_trees: number[][] = parse(input_text);
	const width = array_trees[0].length;
	const height = array_trees.length;
	for (let j = 1; j < array_trees.length - 1; j++) {
		for (let i = 1; i < array_trees[0].length - 1; i++) {
			const element = array_trees[j][i];
			const view = view_quality(i, j, element, array_trees);
			max_visibility = view > max_visibility ? view : max_visibility;
		}
	}
	return max_visibility;
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});
Deno.test('Test p2 example', () => {
	const output_p2 = p2(example);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 1', p1(input));

console.log('Real Imput, part 2', p2(input));
