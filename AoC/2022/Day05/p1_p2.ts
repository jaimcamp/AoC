import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, zip } from '../deps.ts';

class Stack {
	crates: string[];
	id: number;

	constructor(list_crates: string[]) {
		this.crates = list_crates.slice(1);
		this.id = Number(list_crates[0]);
	}
	get_crates(amount: number) {
		const out = this.crates.splice(amount * -1).reverse();
		return out;
	}
	get_crates_9001(amount: number) {
		const out = this.crates.splice(amount * -1);
		return out;
	}
	put_crates(crates_in: string[]) {
		this.crates = this.crates.concat(crates_in);
	}
	get_top() {
		return this.crates[this.crates.length - 1];
	}
}

class Cargo {
	stacks: Map<number, Stack>;
	constructor() {
		this.stacks = new Map<number, Stack>();
	}

	add_stack(new_stack: Stack) {
		this.stacks.set(new_stack.id, new_stack);
	}
	apply_9001(line: number[]) {
		const [amount, from, to]: number[] = line;
		const crates = this.stacks.get(from)!.get_crates_9001(amount);
		this.stacks.get(to)!.put_crates(crates);
	}
	apply(line: number[]) {
		const [amount, from, to]: number[] = line;
		const crates = this.stacks.get(from)!.get_crates(amount);
		this.stacks.get(to)!.put_crates(crates);
	}
	get_top_crates() {
		const out = [];
		for (const value of this.stacks.values()) {
			out.push(value.get_top());
		}
		return out.join('');
	}
}

const parse = (raw_text: string): [string[][], number[][]] => {
	const r = /\d+/g;
	const [state, instructions_all]: string[][] = raw_text.split('\n\n').map((blocks) => blocks.trimEnd().split('\n'));
    /* console.log(instructions_all) */
	const instructions = instructions_all.map((line) => (line.match(r) as [string, string, string]).map(Number));
	const all_crates: string[][] = [];
	for (let index = 0; index < state.length; index++) {
		const line: string[] = state[index].split('');
		const tmp_crates: string[] = [];
		for (let j = 1; j < line.length; j += 4) {
			tmp_crates.push(line[j]);
		}
		all_crates.push(tmp_crates);
	}
	return [all_crates.reverse(), instructions];
};

function p1(input_text: string): string {
	const [data_crates, instructions]: [string[][], number[][]] = parse(input_text);
	const stacks: Cargo = new Cargo();
	(zip(...data_crates) as string[][]).forEach((line) =>
		stacks.add_stack(new Stack(line.filter((val) => val != ' ')))
	);
	for (let index = 0; index < instructions.length; index++) {
		const line = instructions[index];
		/* console.log(line); */
		stacks.apply(line);
		/* for (const value of stacks.stacks.values()) { */
		/* 	console.log(value); */
		/* } */
	}
	return stacks.get_top_crates();
}

function p2(input_text: string): string {
	const [data_crates, instructions]: [string[][], number[][]] = parse(input_text);
	const stacks: Cargo = new Cargo();
	(zip(...data_crates) as string[][]).forEach((line) =>
		stacks.add_stack(new Stack(line.filter((val) => val != ' ')))
	);
	for (let index = 0; index < instructions.length; index++) {
		const line = instructions[index];
		/* console.log(line); */
		stacks.apply_9001(line);
		/* for (const value of stacks.stacks.values()) { */
		/* 	console.log(value); */
		/* } */
	}
	return stacks.get_top_crates();
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});
Deno.test('Test p2 example', () => {
	const output_p2 = p2(example);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 1', p1(input));
console.log('Real Imput, part 2', p2(input));
