import { example, example_p1, example_p2, input } from "./data.ts";
import { assertEquals } from "../deps.ts";

function p1(input_text: string): number {
  const gnomes: string[][] = input_text.trim().split("\n\n").map((line) =>
    line.split("\n")
  );
  const total_food: number[] = [];
  for (let index = 0; index < gnomes.length; index++) {
    const gnome = gnomes[index];
    total_food[index] = gnome.map(Number).reduce((a, b) =>
      a + b
    );
  }
  return Math.max(...total_food);
}

function p2(input_text: string): number {
  const gnomes: string[][] = input_text.trim().split("\n\n").map((line) =>
    line.split("\n")
  );
  const total_food: number[] = [];
  for (let index = 0; index < gnomes.length; index++) {
    const gnome = gnomes[index];
    total_food[index] = gnome.map(Number).reduce((a, b) =>
      a + b
    );
  }
  return total_food.sort((a, b) => b - a).slice(0, 3).reduce((a, b) => a + b);
}

Deno.test("Test p1 example", () => {
  const output_p1 = p1(example);
  assertEquals(example_p1, output_p1);
});
Deno.test("Test p2 example", () => {
  const output_p2 = p2(example);
  assertEquals(example_p2, output_p2);
});

console.log("Real Imput, part 1", p1(input));
console.log("Real Imput, part 2", p2(input));
