export const input: string = Deno.readTextFileSync("input.txt");
export const example = `
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
`;

export const example_p1 = 24000;
export const example_p2 = 45000;
