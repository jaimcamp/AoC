export const input: string = Deno.readTextFileSync("input.txt");
export const example = `
A Y
B X
C Z
`;

export const example_p1 = 15;
export const example_p2 = 12;
