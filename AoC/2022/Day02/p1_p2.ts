import { example, example_p1, example_p2, input } from "./data.ts";
import { assertEquals } from "../deps.ts";

const fix_game = (playerA: string, result: string): string => {
  if (result == "draw") {
    return playerA;
  } else {
    switch (playerA) {
      case "paper":
        return result == "win" ? "scissors" : "rock";
      case "scissors":
        return result == "win" ? "rock" : "paper";
      case "rock":
        return result == "win" ? "paper" : "scissors";
      default:
        console.log("Error");
        return "nothing";
    }
  }
};

const play_game = (playerA: string, playerB: string): number => {
  if (playerA == playerB) {
    return 3;
  } else {
    switch (playerA) {
      case "paper":
        return playerB == "scissors" ? 6 : 0;
      case "scissors":
        return playerB == "rock" ? 6 : 0;
      case "rock":
        return playerB == "paper" ? 6 : 0;
      default:
        console.log("Error");
        return 0;
    }
  }
};

const RPS: Record<string, string> = {
  A: "rock",
  B: "paper",
  C: "scissors",
  X: "rock",
  Y: "paper",
  Z: "scissors",
};

const p2RPS: Record<string, string> = {
  A: "rock",
  B: "paper",
  C: "scissors",
  X: "lose",
  Y: "draw",
  Z: "win",
};

const selectionPoints: Record<string, number> = {
  rock: 1,
  paper: 2,
  scissors: 3,
  lose: 0,
  draw: 3,
  win: 6,
};

function p1(input_text: string): number {
  let runningTotal = 0;
  const games: [string, string][] = input_text.trim().split("\n").map(
    (line) => {
      const return_tuple: [string, string] = line.split(" ").map((val) =>
        RPS[val]
      ) as [string, string];
      return return_tuple;
    },
  );
  for (let index = 0; index < games.length; index++) {
    const element = games[index];
    runningTotal += play_game(...element);
    runningTotal += selectionPoints[element[1]];
  }
  return runningTotal;
}

function p2(input_text: string): number {
  let runningTotal = 0;
  const games: string[][] = input_text.trim().split("\n").map((line) =>
    line.split(" ").map((val) => p2RPS[val])
  );
  for (let index = 0; index < games.length; index++) {
    const element = games[index];
    const playerB = fix_game(element[0], element[1]);
    runningTotal += selectionPoints[element[1]];
    runningTotal += selectionPoints[playerB];
  }
  return runningTotal;
}

Deno.test("Test p1 example", () => {
  const output_p1 = p1(example);
  assertEquals(example_p1, output_p1);
});
Deno.test("Test p2 example", () => {
  const output_p2 = p2(example);
  assertEquals(example_p2, output_p2);
});

console.log("Real Imput, part 1", p1(input));
console.log("Real Imput, part 2", p2(input));
