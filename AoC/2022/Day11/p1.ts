import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, zip } from '../deps.ts';

class Monkey {
	id: number;
	queue: number[];
	operation: string;
	operation_val: number;
	test: number;
	if_true: number;
	if_false: number;
	total_inspections: number;
	constructor(instruction: string[]) {
		const _r = /\d+/g;
		this.id = Number(instruction[0].match(_r)!) as number;
		this.queue = instruction[1].match(_r)!.map(Number);
		this.test = Number(instruction[3].match(_r));
		this.if_true = Number(instruction[4].match(_r));
		this.if_false = Number(instruction[5].match(_r));
		if (instruction[2].match(_r) === undefined) {
			this.operation = instruction[2].split(' ')[4];
			this.operation_val = -1;
		} else {
			this.operation = instruction[2].split(' ')[4];
			this.operation_val = Number(instruction[2].match(_r));
		}
		this.total_inspections = 0;
	}
	get_item(): number | undefined {
		this.total_inspections++;
		return this.queue.shift();
	}
	push_item(current_item: number) {
		return this.queue.push(current_item);
	}
	apply_new(value: number): number {
		switch (this.operation) {
			case '*':
				return this.operation_val > 0 ? value * this.operation_val : value * value;
			case '+':
				return this.operation_val > 0 ? value + this.operation_val : value + value;
			default:
				console.log('Wrong operation');
				return 0;
		}
	}
}

function parse(raw_text: string): string[][] {
	return raw_text.trim().split('\n\n').map((monkey) =>
		monkey.split('\n').map((instruction) => instruction.trim())
	);
}

function p1(input_text: string): number {
	const instructions = parse(input_text);
	const monkeys: Monkey[] = [];
	for (const instruction of instructions) {
		monkeys.push(new Monkey(instruction));
	}
	console.log(monkeys);
	let round = 1;
	while (round <= 20) {
		for (const monkey of monkeys) {
			while (monkey.queue.length) {
				let current_item = monkey.get_item();
				if (current_item === undefined) {
					continue;
				} else {
					current_item = monkey.apply_new(current_item);
					current_item = Math.floor(current_item / 3);
					if ((current_item % monkey.test) === 0) {
						monkeys[monkey.if_true].push_item(current_item);
						/* console.log(monkey.id, current_item, monkeys[monkey.if_true].queue); */
					} else {
						monkeys[monkey.if_false].push_item(current_item);
						/* console.log(monkey.id, current_item, monkeys[monkey.if_false].queue); */
					}
				}
			}
		}
		/* console.table(monkeys.map((mon) => mon.queue)); */
		/* const output = monkeys.map((mon) => mon.total_inspections).sort((a, b) => b - a).slice(0, 2).reduce((a, b) => */
		/* a * b */
		/* ); */
		round++;
	}
	console.log(monkeys.map((mon) => mon.total_inspections));
	const output = monkeys.map((mon) => mon.total_inspections).sort((a, b) => b - a).slice(0, 2)
		.reduce((a, b) => a * b);
	return output;
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});
/* Deno.test('Test p2 example', () => { */
/* 	const output_p2 = p2(example); */
/* 	assertEquals(example_p2, output_p2); */
/* }); */

console.log('Real Imput, part 1', p1(input));
/* console.log('Real Imput, part 2', p2(input)); */
