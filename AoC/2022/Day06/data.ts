export const input: string = Deno.readTextFileSync('input.txt');
export const example = `
mjqjpqmgbljsphdztnvjfqwrcgsmlb
bvwbjplbgvbhsrlpgdmjqwftvncz
nppdvjthqldpwncqszvftbrmjlhg
nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg
zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw
`;

export const example_p1 = '7,5,6,10,11';
export const example_p2 = '19,23,23,29,26';
