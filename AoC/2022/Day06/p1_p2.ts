import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, zip } from '../deps.ts';

const example_parse = (example_raw: string) => {
	return example_raw.trim().split('\n').map((line) => line.trim());
};

function all_different(element: string[]) {
	const element_set = [...new Set(element)];
	return element.length === element_set.length;
}
function p1(input_text: string) {
	const text = input_text.split('');
	for (let index = 0; index < text.length - 3; index++) {
		const element = text.slice(index, index + 4);
		if (all_different(element)) {
			return index + 4;
		}
	}
}

function p2(input_text: string) {
	const text = input_text.split('');
	for (let index = 0; index < text.length - 13; index++) {
		const element = text.slice(index, index + 14);
		if (all_different(element)) {
			return index + 14;
		}
	}
}

Deno.test('Test p1 example', () => {
	const output_p1 = example_parse(example).map((line) => p1(line)).join(',');
	assertEquals(example_p1, output_p1);
});

Deno.test('Test p2 example', () => {
	const output_p2 = example_parse(example).map((line) => p2(line)).join(',');
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 1', p1(input));

console.log('Real Imput, part 2', p2(input));
