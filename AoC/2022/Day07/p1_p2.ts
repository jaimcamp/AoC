import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals } from '../deps.ts';

class File {
	name: string;
	size: number;
	containing_path: string[];

	constructor(name: string, size: number, folder: string[]) {
		this.name = name;
		this.size = size;
		this.containing_path = folder;
	}
}

class Directory {
	name: string;
	size: number;
	children: Map<string, Directory | File>;
	constructor(name: string) {
		this.name = name;
		this.size = 0;
		this.children = new Map<string, Directory | File>();
	}
	add_directory(dir_name: string) {
		this.children.set(dir_name, new Directory(dir_name));
	}
	add_file(info_element: string, path: string[]) {
		const [size, file_name] = info_element.split(' ');
		this.children.set(file_name, new File(file_name, Number(size), path));
	}
}

class FileSystem {
	root_dir: Directory;
	current_path: string[];
	constructor() {
		this.root_dir = new Directory('/');
		this.current_path = ['/'];
	}
	parse_and_execute(instruccion: string[]) {
		const command = instruccion[0];
		switch (command) {
			case 'cd ..':
				if (this.current_path.length > 1) {
					this.current_path.pop();
				}
				break;
			case 'cd /':
				this.current_path = ['/'];
				break;
			case 'ls':
				this.process_ls(instruccion.slice(1));
				break;
			default: {
				/* Add folder to the path and check that is already in a subfolder */
				const folder_cd = instruccion[0].split(' ')[1];
				this.current_path.push(folder_cd);
				/* this.root_dir.check_folder([...this.current_path.slice(1)]); */
				break;
			}
		}
		if (isNumeric(instruccion[0])) {
			const file = new File(instruccion[1], Number(instruccion[0]), this.current_path);
		}
	}
	process_ls(elements: string[]) {
		for (const element of elements) {
			const [type_element, info_element] = element.split(' ');
			switch (type_element) {
				case 'dir':
					this.create_folder(info_element);
					break;

				default:
					this.create_file(element);
					break;
			}
		}
	}
	create_file(size_name_file: string) {
		const [size, _] = size_name_file.split(' ');
		const parent_path_folder = this.current_path[this.current_path.length - 1];
		const parent_directory = this.search_tree(this.root_dir, this.current_path.join('#'));
		parent_directory!.add_file(size_name_file, [...this.current_path]);
		for (let index = 0; index < this.current_path.length; index++) {
			this.search_tree(this.root_dir, this.current_path.slice(0, index + 1).join('#'))!.size += Number(size);
		}
	}

	search_tree(tree: Directory, target: string): Directory | undefined {
		if (tree.name === target) {
			return tree;
		}
		for (const [key, value] of tree.children) {
			if (value instanceof Directory) {
				if (key === target) {
					return value;
				}
				const sub = this.search_tree(value, target);
				if (sub) return sub;
			}
		}
	}
	create_folder(new_dir_name: string) {
		const parent_path_folder = this.current_path[this.current_path.length - 1];
		const parent_directory = this.search_tree(this.root_dir, this.current_path.join('#'));
		const full_name: string = [...this.current_path, new_dir_name].join('#');
		parent_directory!.add_directory(full_name);
	}

	search_all_directories(tree: Directory, acc: number[]) {
		for (const [key, value] of tree.children) {
			if (value instanceof Directory) {
				this.search_all_directories(value, acc);
			}
		}
		acc.push(tree.size);
		return tree.size;
	}

	search_all_directories_p2(tree: Directory, acc: number[], dirs_acc: string[]) {
		for (const [key, value] of tree.children) {
			if (value instanceof Directory) {
				this.search_all_directories_p2(value, acc, dirs_acc);
			}
		}
		acc.push(tree.size);
		dirs_acc.push(tree.name);
		return tree.size;
	}
	calculate_p1() {
		const acc: number[] = [];
		this.search_all_directories(this.root_dir, acc);
		return acc.filter((val) => val < 100000).reduce((a, b) => a + b);
	}

	calculate_p2() {
		const acc: number[] = [];
		const dirs_acc: string[] = [];
		this.search_all_directories_p2(this.root_dir, acc, dirs_acc);
		const to_free = 30000000 - (70000000 - this.root_dir.size);
		return acc.filter((val) => val > to_free).sort((a, b) => a - b)[0];
	}
}

function isNumeric(value: string) {
	return /^-?\d+$/.test(value);
}

function p1(input_text: string) {
	const fs = new FileSystem();
	const instruccions = input_text.trim().split('$').filter((val) => val.length > 0).map((line) =>
		line.trim().split('\n').filter((val) => val != '')
	);
	for (let index = 0; index < instruccions.length; index++) {
		const instruccion = instruccions[index];
		fs.parse_and_execute(instruccion);
	}
	return fs.calculate_p1();
}
function p2(input_text: string) {
	const fs = new FileSystem();
	const instruccions = input_text.trim().split('$').filter((val) => val.length > 0).map((line) =>
		line.trim().split('\n').filter((val) => val != '')
	);
	for (let index = 0; index < instruccions.length; index++) {
		const instruccion = instruccions[index];
		fs.parse_and_execute(instruccion);
	}
	return fs.calculate_p2();
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});

Deno.test('Test p2 example', () => {
	const output_p2 = p2(example);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 1', p1(input));

console.log('Real Imput, part 2', p2(input));
