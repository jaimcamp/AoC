import { example, example_2, example_p1, example_p2, input } from './data.ts';
import { assert, assertEquals, cloneDeep, isEqual } from '../deps.ts';

type Point = {
	x: number;
	y: number;
	steps: number;
};

function cartesianProduct<T>(...allEntries: T[][]): T[][] {
	return allEntries.reduce<T[][]>(
		(results, entries) =>
			results.map((result) => entries.map((entry) => [...result, entry])).reduce(
				(subResults, result) => [...subResults, ...result],
				[],
			),
		[[]],
	);
}

const x_movs = [-1, 0, 1];
const y_movs = [-1, 0, 1];
const possible_movs = cartesianProduct(x_movs, y_movs);

function distance_points(H: Point, T: Point) {
	return Math.sqrt(
		Math.pow(H.x - T.x, 2) +
			Math.pow(H.y - T.y, 2),
	);
}

function optimal_movement(H: Point, T: Point) {
	let min_distance = Infinity;
	let opt_movement: number[] = [];
	for (const movement of possible_movs) {
		const T_temp: Point = cloneDeep(T);
		T_temp.x += movement[0];
		T_temp.y += movement[1];
		const move_distance = distance_points(H, T_temp);
		if (move_distance < min_distance) {
			min_distance = move_distance;
			opt_movement = movement;
		}
	}
	return opt_movement;
}

function apply_step(instruction: [string, number], H: Point, T: Point, visitedTiles: Set<string>) {
	for (let index = 0; index < instruction[1]; index++) {
		switch (instruction[0]) {
			case 'R':
				H.x++;
				break;
			case 'L':
				H.x--;
				break;
			case 'U':
				H.y++;
				break;
			case 'D':
				H.y--;
				break;
			default:
				break;
		}
		H.steps++;
		if (distance_points(H, T) >= 2) {
			const T_mov = optimal_movement(H, T);
			if (!isEqual(T_mov, [0, 0])) {
				T.x += T_mov[0];
				T.y += T_mov[1];
				T.steps++;
				visitedTiles.add([T.x, T.y].join('|'));
			}
		}
	}
}
function apply_step_p2(instruction: [string, number], rope: Point[], visitedTiles: Set<string>) {
	const H = rope[0];
	for (let index = 0; index < instruction[1]; index++) {
		switch (instruction[0]) {
			case 'R':
				H.x++;
				break;
			case 'L':
				H.x--;
				break;
			case 'U':
				H.y++;
				break;
			case 'D':
				H.y--;
				break;
			default:
				break;
		}
		H.steps++;
		for (let index = 0; index < rope.length - 1; index++) {
			const pair = rope.slice(index, index + 2);
			assert(pair.length == 2);
			const [P1, P2] = pair;
			if (distance_points(P1, P2) >= 2) {
				const T_mov = optimal_movement(P1, P2);
				if (!isEqual(T_mov, [0, 0])) {
					P2.x += T_mov[0];
					P2.y += T_mov[1];
					P2.steps++;
				}
			}
		}
		visitedTiles.add([rope[rope.length - 1].x, rope[rope.length - 1].y].join('|'));
	}
}

function p1(input_text: string): number {
	const instructions = input_text.trim().split('\n').map((line) => line.trim().split(' '));
	const visitedTiles = new Set<string>();
	visitedTiles.add([0, 0].join('|'));
	const H: Point = { x: 0, y: 0, steps: 0 };
	const T: Point = { x: 0, y: 0, steps: 0 };
	for (const instruction of instructions) {
		apply_step([instruction[0], Number(instruction[1])], H, T, visitedTiles);
		/* console.log(instruction, H, T); */
	}
	/* console.log(visitedTiles); */

	return visitedTiles.size;
}

function p2(input_text: string): number {
	const instructions = input_text.trim().split('\n').map((line) => line.trim().split(' '));
	const visitedTiles = new Set<string>();
	visitedTiles.add([0, 0].join('|'));
	const H: Point = { x: 0, y: 0, steps: 0 };
	const P_1: Point = { x: 0, y: 0, steps: 0 };
	const P_2: Point = { x: 0, y: 0, steps: 0 };
	const P_3: Point = { x: 0, y: 0, steps: 0 };
	const P_4: Point = { x: 0, y: 0, steps: 0 };
	const P_5: Point = { x: 0, y: 0, steps: 0 };
	const P_6: Point = { x: 0, y: 0, steps: 0 };
	const P_7: Point = { x: 0, y: 0, steps: 0 };
	const P_8: Point = { x: 0, y: 0, steps: 0 };
	const P_9: Point = { x: 0, y: 0, steps: 0 };
	const rope = [H, P_1, P_2, P_3, P_4, P_5, P_6, P_7, P_8, P_9];
	for (const instruction of instructions) {
		apply_step_p2([instruction[0], Number(instruction[1])], rope, visitedTiles);
		/* console.log(instruction, rope, visitedTiles); */
	}

	return visitedTiles.size;
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});
Deno.test('Test p2 example', () => {
	const output_p2 = p2(example_2);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 1', p1(input));

console.log('Real Imput, part 2', p2(input));
