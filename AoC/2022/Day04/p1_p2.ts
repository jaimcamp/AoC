import { example, example_p1, example_p2, input } from './data.ts';
import { assertEquals, intersection } from '../deps.ts';

const range = (start_stop: string): number[] => {
	const [start, stop]: [number, number] = start_stop.split('-').map(Number) as [number, number];
	const step = 1;
	return Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + i * step);
};

const parse_input = (input_text: string) => {
	const pairs: string[][] = input_text.trim().split('\n').map((line) => line.split(','));
	return pairs;
};

const expanded_pairs = (raw_pairs: string[][]): number[][][] => {
	const ranges: number[][][] = raw_pairs.map((pair) => {
		return pair.map((gnome) => range(gnome));
	});
	return ranges;
};

function get_min_size(range_pairs: number[][][]): number[] {
	return range_pairs.map((line) => Math.min(...(line.map((val) => val.length))));
}
function p1(input_text: string): number {
	let total_in = 0;
	const raw_pairs: string[][] = parse_input(input_text);
	const range_pairs: number[][][] = expanded_pairs(raw_pairs);
	const intersection_pairs = range_pairs.map((pairs) => intersection(...pairs) as number[]);
	const min_size_pairs = get_min_size(range_pairs);
	intersection_pairs.forEach((element, index) => element.length === min_size_pairs[index] ? total_in++ : null);
	return total_in;
}

function p2(input_text: string): number {
	let total_in = 0;
	const raw_pairs: string[][] = parse_input(input_text);
	const range_pairs: number[][][] = expanded_pairs(raw_pairs);
	const intersection_pairs = range_pairs.map((pairs) => intersection(...pairs) as number[]);
	const min_size_pairs = get_min_size(range_pairs);
	intersection_pairs.forEach((element, index) => element.length > 0 ? total_in++ : null);
	return total_in;
}

Deno.test('Test p1 example', () => {
	const output_p1 = p1(example);
	assertEquals(example_p1, output_p1);
});
Deno.test('Test p2 example', () => {
	const output_p2 = p2(example);
	assertEquals(example_p2, output_p2);
});

console.log('Real Imput, part 1', p1(input));
console.log('Real Imput, part 2', p2(input));
