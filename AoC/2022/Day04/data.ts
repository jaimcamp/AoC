export const input: string = Deno.readTextFileSync("input.txt");
export const example = `
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`;

export const example_p1 = 2;
export const example_p2 = 4;
