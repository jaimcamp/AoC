#!/usr/bin/env ts-node
"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
var filename = 'input.txt';
//let filename: string = 'example2.txt'
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename))
    .toString()
    .trim()
    .split("\n\n").map(function (line) { return line.split('\n'); });
var enhancementAlgo = input[0][0].split("");
var inImage = input[1].map(function (x) { return x.split(""); });
var toInt = {
    '#': 1,
    '.': 0
};
var paddingImg = function (arr, char) {
    if (char === void 0) { char = '.'; }
    var out = __spreadArray(__spreadArray(__spreadArray([], Array(100).fill("").map(function (x) { return Array(arr.length).fill(char); }), true), arr, true), Array(100).fill("").map(function (x) { return Array(arr.length).fill(char); }), true);
    out = out.map(function (row) { return __spreadArray(__spreadArray(__spreadArray([], Array(100).fill(char), true), row, true), Array(100).fill(char), true); });
    return out;
};
var findingNeighbors = function (arr, i, j, char) {
    if (char === void 0) { char = '.'; }
    var rowLimit = arr.length - 1;
    var columnLimit = arr[0].length - 1;
    var output = [];
    for (var x = i - 1; x <= i + 1; x++) {
        for (var y = j - 1; y <= j + 1; y++) {
            if (x < 1 || x > rowLimit - 1 || y < 1 || y > columnLimit - 1) {
                output.push(char);
            }
            else {
                output.push(arr[x][y]);
            }
        }
    }
    return output;
};
var enhance = function (arr, char) {
    if (char === void 0) { char = "."; }
    var outIndex = Array(arr.length).fill("").map(function (line) { return (Array(arr[0].length).fill(0)); });
    for (var col = 0, len = arr[0].length; col < len; col++) {
        for (var row = 0, len2 = arr.length; row < len2; row++) {
            outIndex[row][col] = parseInt(findingNeighbors(arr, row, col, char).map(function (x) { return toInt[x]; }).join(""), 2);
        }
    }
    return outIndex.map(function (row) { return row.map(function (col) { return enhancementAlgo[col]; }); });
};
inImage = paddingImg(inImage);
var outImage = enhance(inImage);
for (var i = 1, len = 50; i < len; i++) {
    outImage = i % 2 === 0 ? enhance(outImage) : enhance(outImage, "#");
}
var nLit = 0;
outImage.forEach(function (row) { return row.forEach(function (val) { return val === '#' ? nLit++ : 0; }); });
console.log(nLit);
