#!/usr/bin/env ts-node


import { readFileSync } from "fs";
import { join } from "path";
import * as _ from "lodash";
import { filter, zeros, subset, setDistinct, setIntersect, index, distance, matrix } from "mathjs";

let filename: string = 'input.txt'
//let filename: string = 'example2.txt'
let input = readFileSync(join(__dirname, filename))
    .toString()
    .trim()
    .split("\n\n").map((line) => line.split('\n'));


let enhancementAlgo = input[0][0].split("");
let inImage = input[1].map((x) => x.split(""));

let toInt: { [index: string]: number } = {
    '#': 1,
    '.': 0
}

const paddingImg = (arr: string[][], char: string = '.'): string[][] => {

    let out = [...Array(100).fill("").map((x) => Array(arr.length).fill(char)),
    ...arr,
    ...Array(100).fill("").map((x) => Array(arr.length).fill(char))]
    out = out.map((row) => [...Array(100).fill(char), ...row, ...Array(100).fill(char)])
    return out
}

const findingNeighbors = (arr: string[][], i: number, j: number, char: string = '.') => {
    let rowLimit = arr.length - 1;
    let columnLimit = arr[0].length - 1;
    let output: string[] = [];

    for (var x = i - 1; x <= i + 1; x++) {
        for (var y = j - 1; y <= j + 1; y++) {
            if (x < 1 || x > rowLimit - 1 || y < 1 || y > columnLimit - 1) {
                output.push(char)
            }
            else {
                output.push(arr[x][y])
            }
        }
    }
    return output
}

const enhance = (arr: string[][], char: string = "."): string[][] => {

    let outIndex: number[][] = Array(arr.length).fill("").map((line) => (Array(arr[0].length).fill(0)))
    for (let col = 0, len = arr[0].length; col < len; col++) {
        for (let row = 0, len2 = arr.length; row < len2; row++) {
            outIndex[row][col] = parseInt(findingNeighbors(arr, row, col, char).map((x) => toInt[x]).join(""), 2);
        }
    }
    return outIndex.map((row) => row.map((col) => enhancementAlgo[col]));
}

inImage = paddingImg(inImage);
let outImage = enhance(inImage);
for (let i = 1, len = 50; i < len; i++) {
    outImage = i % 2 === 0 ? enhance(outImage) : enhance(outImage, "#")
}
let nLit = 0;
outImage.forEach((row) => row.forEach((val) => val === '#' ? nLit++ : 0))


console.log(nLit);
