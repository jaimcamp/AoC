#!/usr/bin/env ts-node
"use strict";
exports.__esModule = true;
var mathjs_1 = require("mathjs");
var input = "--- scanner 0 ---\n0, 2\n4, 1\n3, 3\n\n--- scanner 1 -- -\n-1, -1\n- 5, 0\n- 2, 1".toString()
    .trim()
    .split("\n\n")
    .map(function (block) { return block.split('\n'); });
var Scanner = /** @class */ (function () {
    function Scanner(name, beacons) {
        this.name = name;
        this.beacons = [];
        this.sharedBeacons = new Set();
        for (var _i = 0, beacons_1 = beacons; _i < beacons_1.length; _i++) {
            var beaconString = beacons_1[_i];
            var _a = beaconString.split(",").map(Number), x = _a[0], y = _a[1], z = _a[2];
            var beacon = {
                posX: x,
                posY: y,
                posZ: z
            };
            this.beacons.push(beacon);
        }
    }
    Scanner.prototype.beaconDistance = function () {
        this.beaconsdistance = (0, mathjs_1.zeros)([this.beacons.length, this.beacons.length]);
        for (var i = 0, len = this.beacons.length; i < len; i++) {
            for (var j = 0, len2 = this.beacons.length; j < len2; j++) {
                if (j > i) {
                    var dist = (0, mathjs_1.distance)(Object.values(this.beacons[i]).splice(0, 3), Object.values(this.beacons[j]).splice(0, 3));
                    this.beaconsdistance = (0, mathjs_1.subset)(this.beaconsdistance, (0, mathjs_1.index)(i, j), dist.toFixed(3));
                }
            }
        }
        return this.beaconsdistance;
    };
    Scanner.prototype.plot = function () {
        var canvas = (0, mathjs_1.zeros)([
            Math.abs(Math.max.apply(Math, this.beacons.map(function (beacon) { return beacon.posX; })) - Math.min.apply(Math, this.beacons.map(function (beacon) { return beacon.posX; }))) + 1,
            Math.abs(Math.max.apply(Math, this.beacons.map(function (beacon) { return beacon.posY; })) - Math.min.apply(Math, this.beacons.map(function (beacon) { return beacon.posY; }))) + 1
        ]);
        return canvas;
    };
    return Scanner;
}());
var Scanners = input.map(function (scanner) { return new Scanner(scanner[0], scanner.splice(1)); });
console.log(Scanners);
// function isPositive(x: any) {
//     return x > 0
// }
// const intersectBeacons = (scannerA: Scanner, scannerB: Scanner) => {
//     const sharedA = new Set();
//     const sharedB = new Set();
//     let intersect = filter(
//         setIntersect(
//             scannerA.beaconsdistance!,
//             scannerB.beaconsdistance!
//         ),
//         isPositive
//     ).valueOf() as number[]
//     //if (scannerA.beacons.length != scannerB.beacons.length)
//     //throw new Error(`Different beacons length ${scannerA.beacons.length} and ${scannerB.beacons.length}, names: ${scannerA.name}, ${scannerB.name}`)
//     if (intersect.length >= ((12 - 1) * 12 / 2)) {
//         scannerA.beaconsdistance!.forEach((val, index, arr) => (val as number[])
//             .forEach((a, b, c) => intersect.includes(a as number) ? sharedA.add(index as number).add(b as number) : false))
//         scannerB.beaconsdistance!.forEach((val, index, arr) => (val as number[])
//             .forEach((a, b, c) => intersect.includes(a as number) ? sharedB.add(index as number).add(b as number) : false))
//         let beaconsShared = {
//             scannerA: scannerA.beacons.filter((value, index) => sharedA.has(index)),
//             scannerB: scannerB.beacons.filter((value, index) => sharedB.has(index))
//         }
//         beaconsShared.scannerA.forEach((beacon) => scannerA.sharedBeacons.add(beacon))
//         beaconsShared.scannerB.forEach((beacon) => scannerB.sharedBeacons.add(beacon))
//         for (const value of intersect) {
//             let rr;
//             scannerA.beaconsdistance!.forEach((row, i) => (row as number[])
//                .forEach((num, j) => value===num as number?console.log(i, j):false))
//         }
//     }
// }
// //const compareDistances = (scannerA: Scanner, scannerB: Scanner): boolean => {
// //let intersect = filter(
// //setIntersect(
// //scannerA.beaconsdistance!,
// //scannerB.beaconsdistance!
// //),
// //isPositive
// //).valueOf() as number[]
// ////const triangleSize = scannerA.beacons.length * (scannerA.beacons.length - 1) / 2;
// ////console.log(intersect.length, triangleSize);
// //return intersect.length >= ((12 - 1) * 12 / 2);
// //}
// scanners.forEach((x) => x.beaconDistance())
// for (let i = 0, len = scanners.length; i < len; i++) {
//     for (let j = 0, len = scanners.length; j < len; j++) {
//         if (j > i) {
//             intersectBeacons(scanners[i], scanners[j])
//         }
//     }
// }
// console.log(scanners.map((x) => x.sharedBeacons.size).reduce((a, b) => a + b))
