#!/usr/bin/env ts-node


import { readFileSync } from "fs";
import { join } from "path";
import * as _ from "lodash";
// import { filter, zeros, subset, setDistinct, setIntersect, index, distance } from "mathjs";
import { array } from "vectorious";

let input = `--- scanner 0 ---
0, 2
4, 1
3, 3

--- scanner 1 -- -
-1, -1
- 5, 0
- 2, 1`.toString()
  .trim()
  .split("\n\n")
  .map((block) => block.split('\n'))

//let filename: string = 'input.txt'

// let filename: string = 'example.txt'
// let input: string[][] = readFileSync(join(__dirname, filename))
//   .toString()
//   .trim()
//   .split("\n\n")
//   .map((block) => block.split('\n'))


interface Beacon {
  posX: number;
  posY: number;
  posZ?: number;
}

class Scanner {

  name: string;
  pos?: number[];
  posX?: number;
  posY?: number;
  posZ?: number;
  beacons: Beacon[];
  sharedBeacons: Set<Beacon>;
  beaconsdistance?: math.MathArray;

  constructor(name: string, beacons: string[]) {
    this.name = name;
    this.beacons = [];
    this.sharedBeacons = new Set();
    for (const beaconString of beacons) {
      let [x, y, z] = beaconString.split(",").map(Number)
      const beacon: Beacon = {
        posX: x,
        posY: y,
        posZ: z
      }
      this.beacons.push(beacon)
    }
  }

  beaconDistance() {
    this.beaconsdistance = zeros([this.beacons.length, this.beacons.length]) as math.MathArray
    for (let i = 0, len = this.beacons.length; i < len; i++) {
      for (let j = 0, len2 = this.beacons.length; j < len2; j++) {
        if (j > i) {
          let dist = distance(
            Object.values(this.beacons[i]).splice(0, 3),
            Object.values(this.beacons[j]).splice(0, 3)
          )
          this.beaconsdistance = subset(this.beaconsdistance, index(i, j), dist.toFixed(3))
        }
      }
    }
    return this.beaconsdistance
  }

  plot() {
    let canvas = zeros([
      Math.abs(
        Math.max(...this.beacons.map(beacon => beacon.posX)) -
        Math.min(...this.beacons.map(beacon => beacon.posX))
      ) + 1,
      Math.abs(
        Math.max(...this.beacons.map(beacon => beacon.posY)) -
        Math.min(...this.beacons.map(beacon => beacon.posY))
      ) + 1])
    return canvas
  }
}

const Scanners: Scanner[] = input.map((scanner) => new Scanner(scanner[0], scanner.splice(1)));
console.log(Scanners)

// function isPositive(x: any) {
//     return x > 0
// }

// const intersectBeacons = (scannerA: Scanner, scannerB: Scanner) => {
//     const sharedA = new Set();
//     const sharedB = new Set();
//     let intersect = filter(
//         setIntersect(
//             scannerA.beaconsdistance!,
//             scannerB.beaconsdistance!
//         ),
//         isPositive
//     ).valueOf() as number[]
//     //if (scannerA.beacons.length != scannerB.beacons.length)
//     //throw new Error(`Different beacons length ${scannerA.beacons.length} and ${scannerB.beacons.length}, names: ${scannerA.name}, ${scannerB.name}`)
//     if (intersect.length >= ((12 - 1) * 12 / 2)) {

//         scannerA.beaconsdistance!.forEach((val, index, arr) => (val as number[])
//             .forEach((a, b, c) => intersect.includes(a as number) ? sharedA.add(index as number).add(b as number) : false))
//         scannerB.beaconsdistance!.forEach((val, index, arr) => (val as number[])
//             .forEach((a, b, c) => intersect.includes(a as number) ? sharedB.add(index as number).add(b as number) : false))

//         let beaconsShared = {
//             scannerA: scannerA.beacons.filter((value, index) => sharedA.has(index)),
//             scannerB: scannerB.beacons.filter((value, index) => sharedB.has(index))
//         }

//         beaconsShared.scannerA.forEach((beacon) => scannerA.sharedBeacons.add(beacon))
//         beaconsShared.scannerB.forEach((beacon) => scannerB.sharedBeacons.add(beacon))
//         for (const value of intersect) {
//             let rr;
//             scannerA.beaconsdistance!.forEach((row, i) => (row as number[])
//                .forEach((num, j) => value===num as number?console.log(i, j):false))
//         }
//     }
// }

// //const compareDistances = (scannerA: Scanner, scannerB: Scanner): boolean => {
// //let intersect = filter(
// //setIntersect(
// //scannerA.beaconsdistance!,
// //scannerB.beaconsdistance!
// //),
// //isPositive
// //).valueOf() as number[]
// ////const triangleSize = scannerA.beacons.length * (scannerA.beacons.length - 1) / 2;
// ////console.log(intersect.length, triangleSize);
// //return intersect.length >= ((12 - 1) * 12 / 2);
// //}

// scanners.forEach((x) => x.beaconDistance())
// for (let i = 0, len = scanners.length; i < len; i++) {
//     for (let j = 0, len = scanners.length; j < len; j++) {
//         if (j > i) {
//             intersectBeacons(scanners[i], scanners[j])
//         }
//     }
// }


// console.log(scanners.map((x) => x.sharedBeacons.size).reduce((a, b) => a + b))
