"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
//const vals:number[][] = [
//'00100',
//'11110',
//'10110',
//'10111',
//'10101',
//'01111',
//'00111',
//'11100',
//'10000',
//'11001',
//'00010',
//'01010',
//].map((x) => x.split("").map(Number))
var vals = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, 'input.txt'), 'utf-8').toString().trim().split("\n").map(function (x) { return x.split("").map(Number); });
var fn_sums = function (array) {
    var sums = [];
    var mode = [];
    var threshold = array.length / 2;
    for (var i = 0, len = array[0].length; i < len; i++) {
        var sum_element = 0;
        for (var j = 0, lenj = array.length; j < lenj; j++) {
            sum_element += array[j][i];
        }
        sums.push(sum_element);
    }
    for (var _i = 0, sums_1 = sums; _i < sums_1.length; _i++) {
        var e = sums_1[_i];
        if (e === threshold) {
            mode.push(2);
        }
        else if (e > threshold) {
            mode.push(1);
        }
        else {
            mode.push(0);
        }
    }
    return mode;
};
var ox_arr = vals;
var co2_arr = vals;
var m = 0;
var _loop_1 = function () {
    var first_bit = fn_sums(ox_arr)[m];
    first_bit = first_bit === 2 ? 1 : first_bit;
    ox_arr = ox_arr.filter(function (element) { return element[m] === first_bit ? true : false; });
    m++;
};
while (ox_arr.length > 1) {
    _loop_1();
}
var n = 0;
var _loop_2 = function () {
    var first_bit = fn_sums(co2_arr)[n];
    first_bit = first_bit === 2 ? 1 : first_bit;
    co2_arr = co2_arr.filter(function (element) { return element[n] === first_bit ? false : true; });
    n++;
};
while (co2_arr.length > 1) {
    _loop_2();
}
console.log(parseInt(ox_arr[0].join(''), 2) *
    parseInt(co2_arr[0].join(''), 2));
