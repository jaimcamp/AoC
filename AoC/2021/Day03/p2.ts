import {readFileSync} from 'fs';
import {join} from 'path';

//const vals:number[][] = [
    //'00100',
    //'11110',
    //'10110',
    //'10111',
    //'10101',
    //'01111',
    //'00111',
    //'11100',
    //'10000',
    //'11001',
    //'00010',
    //'01010',
//].map((x) => x.split("").map(Number))

const vals:number[][] = readFileSync(join(__dirname, 'input.txt'), 'utf-8').toString().trim().split("\n").map((x) => x.split("").map(Number))

const fn_sums = function(array:number[][]): number[] {
    const sums:number[] = [];
    const mode:number[] = [];
    const threshold:number = array.length / 2
    for (let i = 0, len = array[0].length; i < len; i++) {
        let sum_element:number = 0;
        for (let j = 0, lenj = array.length; j < lenj; j++) {
            sum_element += array[j][i]
        }
        sums.push(sum_element)
    }
    for (const e of sums){
        if (e === threshold) {
            mode.push(2)
        }
        else if (e > threshold) {
            mode.push(1)
        }
        else {
            mode.push(0)
        }
    }
    return mode
}

let ox_arr = vals;
let co2_arr = vals;

let m = 0;
while (ox_arr.length > 1){
    let first_bit:number = fn_sums(ox_arr)[m]
    first_bit = first_bit===2?1:first_bit
    ox_arr = ox_arr.filter(element =>  element[m]===first_bit?true:false )
    m++
}

let n = 0;
while (co2_arr.length > 1){
    let first_bit:number = fn_sums(co2_arr)[n]
    first_bit = first_bit===2?1:first_bit
    co2_arr = co2_arr.filter(element =>  element[n]===first_bit?false:true )
    n++
}
console.log(
    parseInt(ox_arr[0].join(''), 2) *
        parseInt(co2_arr[0].join(''), 2)
)
