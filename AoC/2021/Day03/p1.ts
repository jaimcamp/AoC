import {readFileSync} from 'fs';
import {join} from 'path';

//const vals:number[][] = [
    //'00100',
    //'11110',
    //'10110',
    //'10111',
    //'10101',
    //'01111',
    //'00111',
    //'11100',
    //'10000',
    //'11001',
    //'00010',
    //'01010',
//].map((x) => x.split("").map(Number))

const vals:number[][] = readFileSync(join(__dirname, 'input.txt'), 'utf-8').toString().trim().split("\n").map((x) => x.split("").map(Number))

const sums:number[] = [];
const threshold:number = Math.ceil(vals.length / 2);

for (let i = 0, len = vals[0].length; i < len; i++) {
    let sum_element:number = 0;
    for (let j = 0, lenj = vals.length; j < lenj; j++) {
        sum_element += vals[j][i]
    }
    sums.push(sum_element)
}

const gamma:number[] = [];
const epsilon:number[] = [];
for (const e of sums){
    if (e >= threshold) {
        gamma.push(1);
        epsilon.push(0);
    }
    else {
        gamma.push(0);
        epsilon.push(1);
    }
}
console.log(parseInt(gamma.join(''), 2) *
            parseInt(epsilon.join(''), 2))
