import {readFileSync} from 'fs';

//const vals:string[] = [
//'forward 5',
//'down 5',
//'forward 8',
//'up 3',
//'down 8',
//'forward 2',
//]
const vals:string[] = readFileSync('Day02/input.txt', 'utf-8').toString().trim().split("\n")
let split = vals.map((x) => x.split(" "));
let x_pos = 0;
let y_pos = 0;

for (let i = 0, len = split.length; i < len; i++) {
    if (split[i][0] == 'forward') {
        x_pos += parseInt(split[i][1], 10)
    }
    else if (split[i][0] == 'down') {
        y_pos += parseInt(split[i][1], 10)
    }
    else if (split[i][0] == 'up') {
        y_pos -= parseInt(split[i][1], 10)
    }
}


console.log(x_pos * y_pos)
