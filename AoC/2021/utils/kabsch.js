"use strict";
exports.__esModule = true;
var vectorious_1 = require("vectorious");
var lodash_1 = require("lodash");
var svd_js_1 = require("svd-js");
var mathjs_1 = require("mathjs");
var one = (0, vectorious_1.array)([[23, 178],
    [66, 173],
    [88, 187],
    [119, 202],
    [122, 229],
    [170, 232],
    [179, 199]]);
var two = (0, vectorious_1.array)([[232, 38],
    [208, 32],
    [181, 31],
    [155, 45],
    [142, 33],
    [121, 59],
    [139, 69]]);
function mean_0(M) {
    var b = M.reduce(function (acc, cur) {
        cur.forEach(function (e, i) { return acc[i] = acc[i] ? acc[i] + e : e; });
        return acc;
    }, []).map(function (e) { return e / M.length; });
    return b;
}
function kabsch_umeyama(A, B) {
    var _a = A.shape, n = _a[0], m = _a[1];
    console.assert((0, lodash_1.isEqual)(A.shape, B.shape));
    var EA = mean_0(A.toArray());
    var EB = mean_0(B.toArray());
    var diffA = A.toArray().map(function (row) { return row.map(function (num, idx) {
        return num - EA[idx];
    }); });
    var diffB = B.toArray().map(function (row) { return row.map(function (num, idx) {
        return num - EB[idx];
    }); });
    var VarA = (0, lodash_1.mean)(diffA.map(function (row) { return Math.pow((0, vectorious_1.norm)(row), 2); }));
    var TdiffA = (0, vectorious_1.array)(diffA).T;
    var TdiffB = (0, vectorious_1.array)(diffB);
    var H = TdiffA.multiply(TdiffB).map(function (value) { return value / n; });
    var _b = (0, svd_js_1.SVD)(H.toArray()), u = _b.u, v = _b.v, q = _b.q;
    var du = (0, vectorious_1.array)(u).det();
    var dv = (0, vectorious_1.array)(v).det();
    var d = Math.abs(du * dv);
    var S = (0, mathjs_1.diag)(new Array(m - 1).fill(1).concat(d));
    var R = (0, vectorious_1.array)(u).multiply((0, vectorious_1.array)(S)).multiply((0, vectorious_1.array)(v).T);
    var c = VarA / ((0, vectorious_1.array)((0, mathjs_1.diag)(q)).multiply((0, vectorious_1.array)(S))).trace();
    var t = (0, mathjs_1.subtract)(EA, (0, mathjs_1.multiply)((0, mathjs_1.matrix)(R.toArray()), EB).map(function (value) { return value * c; })).valueOf();
    return { R: R, : .toArray(), c: c, t: t };
}
console.log(kabsch_umeyama(one, two));
