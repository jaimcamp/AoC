import { array, NDArray, norm } from 'vectorious';
import { isEqual, mean } from 'lodash';
import { SVD } from 'svd-js'
import { diag, matrix, multiply, subtract, typeOf } from 'mathjs'

const one = array([[23, 178],
[66, 173],
[88, 187],
[119, 202],
[122, 229],
[170, 232],
[179, 199]])
const two = array([[232, 38],
[208, 32],
[181, 31],
[155, 45],
[142, 33],
[121, 59],
[139, 69]])

function mean_0(M: number[][]): number[] {
  let b = M.reduce((acc, cur) => {
    cur.forEach((e, i) => acc[i] = acc[i] ? acc[i] + e : e);
    return acc;
  }, []).map(e => e / M.length);
  return b
}

function kabsch_umeyama(A: NDArray, B: NDArray): any {
  let [n, m] = A.shape;
  console.assert(isEqual(A.shape, B.shape));
  let EA = mean_0(A.toArray());
  let EB = mean_0(B.toArray());
  let diffA = A.toArray().map(row => row.map(function(num, idx) {
    return num - EA[idx];
  }))
  let diffB = B.toArray().map(row => row.map(function(num, idx) {
    return num - EB[idx];
  }))

  let VarA: number = mean(diffA.map(row => norm(row) ** 2));
  let TdiffA = array(diffA).T
  let TdiffB = array(diffB)
  let H = TdiffA.multiply(TdiffB).map(value => value / n);
  let { u, v, q } = SVD(H.toArray())
  let du = array(u).det()
  let dv = array(v).det()

  let d = Math.abs(du * dv);

  let S = diag(new Array(m - 1).fill(1).concat(d));

  let R = array(u).multiply(array(S)).multiply(array(v).T);
  let c = VarA / (array(diag(q)).multiply(array(S))).trace()
  let t = subtract(EA, multiply(matrix(R.toArray()), EB).map(value => value * c)).valueOf()
  return { R, c, t }
}
console.log(kabsch_umeyama(one, two));
