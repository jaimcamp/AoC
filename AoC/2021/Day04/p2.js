"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
//let filename = 'example.txt'
var filename = 'input.txt';
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename), 'utf-8').toString().trim().split('\n');
var called_numbers = input[0].split(',').map(Number);
var bingo_cards = input.slice(2);
// From https://stackoverflow.com/a/19506234
var range = function (start, stop, step) {
    if (step === void 0) { step = 1; }
    return Array(Math.ceil((stop - start) / step)).fill(start).map(function (x, y) { return x + y * step; });
};
var Bingo = /** @class */ (function () {
    function Bingo(numbers) {
        if (numbers === void 0) { numbers = Array(5).fill('').map(function () { return Array(5).fill(0); }); }
        this.numbers = numbers;
        this.called = Array(numbers.length).fill('').map(function () { return Array(numbers[0].length).fill(0); });
        this.won = false;
    }
    Bingo.prototype.call_number = function (n) {
        for (var i = 0, len = this.numbers.length; i < len; i++) {
            var idx = this.numbers[i].indexOf(n);
            if (idx >= 0) {
                this.called[i][idx] = 1;
                break;
            }
        }
        return this.called;
    };
    Bingo.prototype.sum_unmarked = function () {
        var out = 0;
        for (var i = 0, len = this.called.length; i < len; i++) {
            for (var j = 0, len_1 = this.called[0].length; j < len_1; j++) {
                if (this.called[i][j] === 0) {
                    out += this.numbers[i][j];
                }
            }
        }
        return out;
    };
    Bingo.prototype.got_bingo = function () {
        var row_sum = [];
        var col_sum = [];
        for (var _i = 0, _a = this.called; _i < _a.length; _i++) {
            var row = _a[_i];
            row_sum.push(row.reduce(function (a, b) { return a + b; }));
        }
        for (var i = 0, len = this.called[0].length; i < len; i++) {
            var tmp = 0;
            for (var j = 0, len_2 = this.called.length; j < len_2; j++) {
                tmp += this.called[j][i];
            }
            col_sum.push(tmp);
        }
        var case_bingo;
        var pos;
        if (row_sum.indexOf(5) >= 0) {
            case_bingo = "row";
            pos = row_sum.indexOf(5);
            this.won = true;
        }
        else if (col_sum.indexOf(5) >= 0) {
            case_bingo = "column";
            pos = row_sum.indexOf(5);
            this.won = true;
        }
        else {
            case_bingo = "none";
            pos = -1;
        }
        var out = {
            case_out: case_bingo,
            pos_out: pos
        };
        return out;
    };
    return Bingo;
}());
var split_bingos = function (raw_array) {
    var ranges = range(0, bingo_cards.length, 6);
    var bingos = [];
    for (var _i = 0, ranges_1 = ranges; _i < ranges_1.length; _i++) {
        var index = ranges_1[_i];
        var processed_array = raw_array.slice(index, index + 5).map(function (element) { return element.trim().split(/ +/).map(Number); });
        var bingo_tmp = new Bingo(processed_array);
        bingos.push(bingo_tmp);
    }
    return bingos;
};
var bingos = split_bingos(bingo_cards);
var l_bingo = new Bingo();
var l_n = -1;
var winners = [];
var all_done = false;
var _loop_1 = function (n) {
    bingos.forEach(function (bingo) { return bingo.call_number(n); });
    var status_1 = [];
    for (var k = 0, len = bingos.length; k < len; k++) {
        status_1.push(bingos[k].got_bingo());
        var w = bingos[k].won;
        if (w) {
            if (winners.indexOf(k) === -1) {
                winners.push(k);
            }
        }
    }
    if (bingos.map(function (bingo) { return bingo.won; }).every(function (x) { return x; })) {
        all_done = true;
        console.log(winners);
        l_bingo = bingos[winners[winners.length - 1]];
        l_n = n;
    }
    if (all_done) {
        return "break";
    }
};
for (var _i = 0, called_numbers_1 = called_numbers; _i < called_numbers_1.length; _i++) {
    var n = called_numbers_1[_i];
    var state_1 = _loop_1(n);
    if (state_1 === "break")
        break;
}
console.log(l_bingo, l_bingo.got_bingo(), l_n, l_n * l_bingo.sum_unmarked());
