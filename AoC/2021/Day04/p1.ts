import {readFileSync}  from 'fs';
import {join}  from 'path';

//let filename = 'example.txt'
let filename = 'input.txt'
const input = readFileSync(join(__dirname, filename), 'utf-8').toString().trim().split('\n')

const called_numbers = input[0].split(',').map(Number);
const bingo_cards = input.slice(2)

// From https://stackoverflow.com/a/19506234
const range = (start:number, stop:number, step:number = 1) =>
Array(Math.ceil((stop - start) / step)).fill(start).map((x, y) => x + y * step)

interface bingo_out {
    case_out: string;
    pos_out: number;
}

class Bingo {
    numbers: number[][];
    called: number[][];

    constructor(numbers: number[][] = Array(5).fill('').map(()=> Array(5).fill(0))  ) {
        this.numbers = numbers;
        this.called = Array(numbers.length).fill('').map(()=> Array(numbers[0].length).fill(0));
    }

    call_number(n: number): number[][] {
        let position:number[] = [];
        for (let i = 0, len = this.numbers.length; i < len; i++) {
            let idx = this.numbers[i].indexOf(n);
            if (idx >= 0){
                console.log(i, idx)
                position = [i, idx];
                this.called[position[0]][position[1]] = 1;
                break;
            }
        }
        return this.called;
    }

    sum_unmarked(): number {
        let out:number = 0;
        for (let i = 0, len = this.called.length; i < len; i++) {
            for (let j = 0, len = this.called[0].length; j < len; j++) {
                if (this.called[i][j] === 0){
                    out += this.numbers[i][j];
                }
            }
        }
        return out;
    }

    got_bingo(): bingo_out {
        let row_sum:number[] = [];
        let col_sum:number[] = [];
        for (const row of this.called) {
            row_sum.push(row.reduce((a, b)=> a+b))
        }
        for (let i = 0, len = this.called[0].length; i < len; i++) {
            let tmp = 0;
            for (let j = 0, len = this.called.length; j < len; j++) {
                tmp += this.called[j][i]
            }
            col_sum.push(tmp)
        }

        let case_bingo:string;
        let pos:number;

        if (row_sum.indexOf(5)>=0){
            case_bingo = "row";
            pos = row_sum.indexOf(5);
        }
        else if (col_sum.indexOf(5)>=0){
            case_bingo = "column";
            pos = row_sum.indexOf(5);
        }
        else {
            case_bingo = "none";
            pos = -1;
        }
        let out: bingo_out = {
            case_out:case_bingo,
            pos_out:pos
        }
        return out
    }
}

const split_bingos = (raw_array: string[]):Bingo[] => {
    const ranges = range(0, bingo_cards.length, 6)
    const bingos:Array<Bingo> = [];
    for (const index of ranges) {
        const processed_array:any = raw_array.slice(index, index+5).map((element) => element.trim().split(/ +/).map(Number));
        const bingo_tmp = new Bingo(processed_array)
        bingos.push(bingo_tmp);
    }
    return bingos
}

const bingos = split_bingos(bingo_cards);

//let i = 0;
let w_bingo:Bingo = new Bingo();
let w_n:number= -1;
for (const n of called_numbers){
    let done:boolean = false;
    console.log(n)
    bingos.forEach((bingo) => bingo.call_number(n));
    const status = bingos.map((bingo) => bingo.got_bingo());
    for (let j = 0, len = status.length; j < len; j++) {
        if (status[j].case_out != 'none'){
            done = true;
            w_bingo = bingos[j];
            w_n = n;
        }
        if (done){break;}
    }
    if (done){break;}
    //i++
    //if (i > 11){break;}
}
console.log(w_bingo, w_bingo.got_bingo(), w_n * w_bingo.sum_unmarked())
