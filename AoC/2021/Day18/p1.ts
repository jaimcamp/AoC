#!/usr/bin/env ts-node


import { readFileSync } from "fs";
import { join } from "path";
import * as _ from "lodash";

type NNumber = [number, number][];

//let filename = 'example.txt';
let filename = 'input.txt';

let input: string[] = readFileSync(join(__dirname, filename))
    .toString()
    .trim()
    .split("\n")

//let input = '[[[[[9,8],1],2],3],4]';
//let input = '[7,[6,[5,[4,[3,2]]]]]'
//let input = '[[6,[5,[4,[3,2]]]],1]'
//let input = '[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]'
//let input = '[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]';
//let input = '[[[[0,7],4],[7,[[8,4],9]]],[1,1]]';

const buildTree = (line: string): NNumber => {
    const out: NNumber = [];
    let level = 0;
    for (const char of line) {
        if (char === '[') {
            level++
        } else if (char === ']') {
            level--
        } else if (char === ',') {
            continue;
        } else {
            out.push([parseInt(char, 10), level])
        }
    }
    return out;
}

const explode = (line: NNumber): NNumber => {
    line = line.slice();
    let position = line.map((x) => x[1]).findIndex((element) => element > 4)
    let out: NNumber = []
    if (position === 0) {
        line[position + 2][0] = line[position + 2][0] + line[position + 1][0];
        line[position] = [0, 4]
        out = line.slice(0, 1).concat(line.slice(position + 2));
    } else if (position + 2 === line.length) {
        line[position - 1][0] = line[position - 1][0] + line[position][0];
        line[position] = [0, 4]
        out = line.slice(0, position + 1)
    } else {
        line[position - 1][0] = line[position - 1][0] + line[position][0];
        line[position] = [0, 4]
        line[position + 2][0] = line[position + 2][0] + line[position + 1][0];
        out = line.slice(0, position + 1).concat(line.slice(position + 2))
    }
    return out;
}

const split = (line: NNumber): NNumber => {
    line = line.slice();
    let position = line.map((x) => x[0]).findIndex((element) => element > 9);
    let value = line[position];
    let out: NNumber = line.slice(0, position)
    let newValue: NNumber = [[Math.floor(value[0] / 2), value[1] + 1], [Math.ceil(value[0] / 2), value[1] + 1]]
    out.push(...newValue)
    out = out.concat(line.slice(position + 1))
    return out
}

const aggNNs = (a: NNumber, b: NNumber): NNumber => {
    a.forEach((val) => val[1] += 1)
    b.forEach((val) => val[1] += 1)
    return a.concat(b)
}
const toExplode = (line: NNumber): boolean => {
    let position = line.map((x) => x[1]).findIndex((element) => element > 4)
    return position > -1;
}
const toSplit = (line: NNumber): boolean => {
    let position = line.map((x) => x[0]).findIndex((element) => element > 9);
    return position > -1;
}

const magnitude = (line: NNumber) => {
    line = line.slice();
    for (const level of _.range(4, 0)) {
        let idx = line.findIndex((x) => x[1] === level)
        while (idx > -1) {
            let newValue: NNumber = [[line[idx][0] * 3 + line[idx + 1][0] * 2, level - 1]]
            line = line
                .slice(0, Math.max(0, idx))
                .concat(newValue)
                .concat(line.slice(Math.min(idx + 2)));
            idx = line.findIndex((x) => x[1] === level)
        }
    }
    return line
}

let currentLine: NNumber = buildTree(input[0]);
for (let i = 1, len = input.length; i < len; i++) {
    currentLine = aggNNs(currentLine, buildTree(input[i]))
    let readyExplode: boolean = toExplode(currentLine);
    let readySplit: boolean = toSplit(currentLine);
    while (readyExplode || readySplit) {
        if (readyExplode) {
            currentLine = explode(currentLine);
            readySplit = toSplit(currentLine);
            readyExplode = toExplode(currentLine);
            continue;
        } else if (readySplit) {
            currentLine = split(currentLine);
            readyExplode = toExplode(currentLine);
            readySplit = toSplit(currentLine);
            continue;
        }
    }
}
console.log(currentLine);
console.log(magnitude(currentLine));
