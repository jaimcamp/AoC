"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
//const filename = 'example.txt'
var filename = "input.txt";
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename))
    .toString()
    .trim()
    .split("\n")
    .map(function (line) { return line.trim().split("").map(Number); });
var getNeighbors = function (arr, node) {
    var out = [];
    var changes_y = [node[0] + 1, node[0] - 1];
    var changes_x = [node[1] + 1, node[1] - 1];
    for (var _i = 0, changes_y_1 = changes_y; _i < changes_y_1.length; _i++) {
        var change = changes_y_1[_i];
        if (change >= 0 && change < arr.length) {
            out.push([change, node[1]]);
        }
    }
    for (var _a = 0, changes_x_1 = changes_x; _a < changes_x_1.length; _a++) {
        var change = changes_x_1[_a];
        if (change >= 0 && change < arr[0].length) {
            out.push([node[0], change]);
        }
    }
    return out;
};
var startingPoint = [0, 0];
var finalPoint = [99, 99];
var visited = {};
var tentativeDistance = {};
for (var i = 0, len = input.length; i < len; i++) {
    for (var j = 0, len_1 = input[0].length; j < len_1; j++) {
        visited[[i, j].join(",")] = false;
        tentativeDistance[[i, j].join(",")] = Infinity;
    }
}
tentativeDistance[[startingPoint[0], startingPoint[1]].join(",")] = 0;
var currentNode = startingPoint;
var _loop_1 = function () {
    var neighbors = getNeighbors(input, currentNode);
    console.log(currentNode, neighbors);
    for (var _i = 0, neighbors_1 = neighbors; _i < neighbors_1.length; _i++) {
        var neighbor = neighbors_1[_i];
        if (!visited[neighbor.join(",")]) {
            var newDistance = input[neighbor[0]][neighbor[1]] + tentativeDistance[currentNode.join(",")];
            if (newDistance < tentativeDistance[neighbor.join(",")]) {
                tentativeDistance[neighbor.join(",")] = newDistance;
            }
        }
    }
    visited[currentNode.join(",")] = true;
    var nonVisitedNodes = Object.keys(visited).filter(function (key) { return visited[key] === false; });
    var sortedNodes = Object.entries(tentativeDistance).filter(function (val) { return nonVisitedNodes.includes(val[0]); }).sort(function (_a, _b) {
        var a = _a[1];
        var b = _b[1];
        return a - b;
    });
    currentNode = sortedNodes[0][0].split(",").map(Number);
};
while (currentNode.join(",") != finalPoint.join(",")) {
    _loop_1();
}
console.log(tentativeDistance);
