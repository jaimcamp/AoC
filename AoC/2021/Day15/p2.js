"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
var lodash_1 = __importDefault(require("lodash"));
//const filename = 'example.txt'
var filename = "input.txt";
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename))
    .toString()
    .trim()
    .split("\n")
    .map(function (line) { return line.trim().split("").map(Number); });
var makeInputBig = function (arr) {
    arr = arr.map(function (line) { return line
        .concat(line.map(function (v) { return (v + 1) > 9 ? 1 - (9 - v) : v + 1; }))
        .concat(line.map(function (v) { return (v + 2) > 9 ? 2 - (9 - v) : v + 2; }))
        .concat(line.map(function (v) { return (v + 3) > 9 ? 3 - (9 - v) : v + 3; }))
        .concat(line.map(function (v) { return (v + 4) > 9 ? 4 - (9 - v) : v + 4; })); });
    var out = arr
        .concat(arr.map(function (line) { return line.map(function (v) { return (v + 1) > 9 ? 1 - (9 - v) : v + 1; }); }))
        .concat(arr.map(function (line) { return line.map(function (v) { return (v + 2) > 9 ? 2 - (9 - v) : v + 2; }); }))
        .concat(arr.map(function (line) { return line.map(function (v) { return (v + 3) > 9 ? 3 - (9 - v) : v + 3; }); }))
        .concat(arr.map(function (line) { return line.map(function (v) { return (v + 4) > 9 ? 4 - (9 - v) : v + 4; }); }));
    return out;
};
input = makeInputBig(input);
var getNeighbors = function (arr, node) {
    var out = [];
    var changes_y = [node[0] + 1, node[0] - 1];
    var changes_x = [node[1] + 1, node[1] - 1];
    for (var _i = 0, changes_y_1 = changes_y; _i < changes_y_1.length; _i++) {
        var change = changes_y_1[_i];
        if (change >= 0 && change < arr.length) {
            out.push([change, node[1]]);
        }
    }
    for (var _a = 0, changes_x_1 = changes_x; _a < changes_x_1.length; _a++) {
        var change = changes_x_1[_a];
        if (change >= 0 && change < arr[0].length) {
            out.push([node[0], change]);
        }
    }
    return out;
};
var startingPoint = [0, 0];
var finalPoint = [input.length - 1, input[0].length - 1];
var visited = {};
var tentativeDistance = {};
var Qvect = [];
for (var i = 0, len = input.length; i < len; i++) {
    for (var j = 0, len_1 = input[0].length; j < len_1; j++) {
        visited[[i, j].join(",")] = false;
        tentativeDistance[[i, j].join(",")] = Infinity;
        Qvect.push([i, j].join(","));
    }
}
tentativeDistance[[startingPoint[0], startingPoint[1]].join(",")] = 0;
var currentNode;
while (Qvect.length) {
    var sortedNodes = Qvect.sort(function (a, b) { return tentativeDistance[a] - tentativeDistance[b]; })[0];
    currentNode = sortedNodes.split(",").map(Number);
    Qvect = lodash_1["default"].pull(Qvect, sortedNodes);
    if (sortedNodes === finalPoint.join(",")) {
        break;
    }
    var neighbors = getNeighbors(input, currentNode);
    for (var _i = 0, neighbors_1 = neighbors; _i < neighbors_1.length; _i++) {
        var neighbor = neighbors_1[_i];
        if (Qvect.includes(neighbor.join(","))) {
            var newDistance = input[neighbor[0]][neighbor[1]] + tentativeDistance[currentNode.join(",")];
            if (newDistance < tentativeDistance[neighbor.join(",")]) {
                tentativeDistance[neighbor.join(",")] = newDistance;
            }
        }
    }
    if (Qvect.length % 1000 == 0) {
        console.log(Qvect.length);
    }
}
console.log(tentativeDistance[finalPoint.join(",")]);
