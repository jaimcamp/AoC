import { readFileSync } from "fs";
import { join } from "path";
import _ from "lodash";

//const filename = 'example.txt'
const filename = "input.txt";

let input:number[][] = readFileSync(join(__dirname, filename))
.toString()
.trim()
.split("\n")
.map((line) => line.trim().split("").map(Number));

const makeInputBig = (arr:number[][]): number[][] => {
    arr = arr.map((line) => line
        .concat(line.map((v) => (v+1)>9?1-(9-v):v+1))
        .concat(line.map((v) => (v+2)>9?2-(9-v):v+2))
        .concat(line.map((v) => (v+3)>9?3-(9-v):v+3))
        .concat(line.map((v) => (v+4)>9?4-(9-v):v+4))
    );
    let out = arr
    .concat(arr.map((line) => line.map((v) => (v+1)>9?1-(9-v):v+1)))
    .concat(arr.map((line) => line.map((v) => (v+2)>9?2-(9-v):v+2)))
    .concat(arr.map((line) => line.map((v) => (v+3)>9?3-(9-v):v+3)))
    .concat(arr.map((line) => line.map((v) => (v+4)>9?4-(9-v):v+4)));

    return out
}

input = makeInputBig(input);

const getNeighbors = (arr:number[][], node:number[]):number[][] => {
    const out:number[][] = [];
    const changes_y = [node[0]+1, node[0]-1];
    const changes_x = [node[1]+1, node[1]-1];
    for (const change of changes_y) {
        if (change >= 0 && change < arr.length) {
            out.push([change,node[1]])
        }
    }
    for (const change of changes_x) {
        if (change >= 0 && change < arr[0].length) {
            out.push([node[0], change])
        }
    }
    return out
}

const startingPoint = [0, 0];
const finalPoint = [input.length - 1, input[0].length -1];
const tentativeDistance:{[index:string]:number} = {};
let Qvect:string[] = [];

for (let i = 0, len = input.length; i < len; i++) {
    for (let j = 0, len = input[0].length; j < len; j++) {
        tentativeDistance[[i,j].join(",")] = Infinity;
        Qvect.push([i, j].join(","));
    }
}

tentativeDistance[ [startingPoint[0],startingPoint[1]].join(",") ] = 0;


let currentNode:number[];

while (Qvect.length) {
    let sortedNodes = Qvect.sort((a, b) => tentativeDistance[a] - tentativeDistance[b])[0]
    currentNode = sortedNodes.split(",").map(Number);
    Qvect = _.pull(Qvect, sortedNodes)

    if (sortedNodes === finalPoint.join(",")){
        break
    }

    const neighbors = getNeighbors(input, currentNode);
    for (const neighbor of neighbors) {
        if (Qvect.includes(neighbor.join(","))){
            let newDistance = input[neighbor[0]][neighbor[1]] + tentativeDistance[currentNode.join(",")]
            if (newDistance < tentativeDistance[neighbor.join(",")]){
                tentativeDistance[neighbor.join(",")] =  newDistance
            }
        }
    }
    if (Qvect.length % 1000 == 0){
        console.log(Qvect.length);
    }
}
console.log(tentativeDistance[finalPoint.join(",")]);
