import { readFileSync } from "fs";
import { join } from "path";
import _ from "lodash";

//const filename = 'example.txt'
const filename = "input.txt";

let input:number[][] = readFileSync(join(__dirname, filename))
.toString()
.trim()
.split("\n")
.map((line) => line.trim().split("").map(Number));

const getNeighbors = (arr:number[][], node:number[]):number[][] => {
    const out:number[][] = [];
    const changes_y = [node[0]+1, node[0]-1];
    const changes_x = [node[1]+1, node[1]-1];
    for (const change of changes_y) {
        if (change >= 0 && change < arr.length) {
            out.push([change,node[1]])
        }
    }
    for (const change of changes_x) {
        if (change >= 0 && change < arr[0].length) {
            out.push([node[0], change])
        }
    }
    return out
}

const startingPoint = [0, 0];
const finalPoint = [99, 99];
const visited:{[index:string]:boolean} = {};
const tentativeDistance:{[index:string]:number} = {};

for (let i = 0, len = input.length; i < len; i++) {
    for (let j = 0, len = input[0].length; j < len; j++) {
        visited[[i,j].join(",")] = false;
        tentativeDistance[[i,j].join(",")] = Infinity;
    }
}

tentativeDistance[ [startingPoint[0],startingPoint[1]].join(",") ] = 0;


let currentNode = startingPoint;
while (currentNode.join(",")!= finalPoint.join(",")) {
    const neighbors = getNeighbors(input, currentNode);
    for (const neighbor of neighbors) {
        if (!visited[neighbor.join(",")]){
            let newDistance = input[neighbor[0]][neighbor[1]] + tentativeDistance[currentNode.join(",")]
            if (newDistance < tentativeDistance[neighbor.join(",")]){
                tentativeDistance[neighbor.join(",")] =  newDistance
            }
        }
    }
    visited[currentNode.join(",")] = true;
    let nonVisitedNodes = Object.keys(visited).filter(key => visited[key] === false);
    let sortedNodes = Object.entries(tentativeDistance).filter((val) => nonVisitedNodes.includes(val[0] )).sort(([,a],[,b]) => a-b);
    currentNode = sortedNodes[0][0].split(",").map(Number);
}
console.log(tentativeDistance);
