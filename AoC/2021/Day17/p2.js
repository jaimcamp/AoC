"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
//const filename = 'example.txt'
var filename = "input.txt";
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename))
    .toString()
    .trim()
    .split(": ")
    .map(function (line) { return line.trim().split(", "); });
var _a = input[1].map(function (line) { return line.slice(2).split("..").map(Number); }), xLim = _a[0], yLim = _a[1];
console.log(xLim, yLim);
var xTrajectory = function (x0, vx0, t) {
    var x = x0;
    var vx = vx0;
    for (var i = 1; i <= t; i++) {
        x += vx;
        vx += vx > 0 ? -1 : vx < 0 ? 1 : 0;
    }
    return x;
};
var yTrajectory = function (y0, vy0, t) {
    var y = y0;
    var vy = vy0;
    for (var i = 1; i <= t; i++) {
        y += vy;
        vy += -1;
    }
    return y;
};
var onTarget = function (vx0, vy0, xLim, yLim) {
    var maxY = -Infinity;
    var inside = false;
    var passed = false;
    var i = 0;
    while (!passed) {
        var x = xTrajectory(0, vx0, i);
        var y = yTrajectory(0, vy0, i);
        if (y > maxY) {
            maxY = y;
        }
        if ((x >= xLim[0] && x <= xLim[1]) && (y >= yLim[0] && y <= yLim[1])) {
            inside = true;
            passed = true;
        }
        if ((x > xLim[1]) || (y < yLim[0])) {
            passed = true;
        }
        i++;
    }
    return inside;
};
//let goodCases: number[][] = [];
var total = 0;
for (var i = 0; i < 200; i++) {
    for (var j = -200; j < 200; j++) {
        if (onTarget(i, j, xLim, yLim)) {
            //goodCases.push([i, j])
            total++;
        }
    }
}
console.log(total);
