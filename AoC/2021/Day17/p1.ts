import { readFileSync } from "fs";
import { join } from "path";
import _ from "lodash";

//const filename = 'example.txt'
const filename = "input.txt";

let input: string[][] = readFileSync(join(__dirname, filename))
    .toString()
    .trim()
    .split(": ")
    .map((line) => line.trim().split(", "));

let [xLim, yLim] = input[1].map((line) => line.slice(2).split("..").map(Number));
console.log(xLim, yLim);

const xTrajectory = (x0: number, vx0: number, t: number) => {
    let x: number = x0;
    let vx: number = vx0;
    for (let i = 1; i <= t; i++) {
        x += vx
        vx += vx > 0 ? -1 : vx < 0 ? 1 : 0;
    }
    //console.log(vx0 * t - (t ** 2) / 2 + x0);
    return x
}
const yTrajectory = (y0: number, vy0: number, t: number) => {
    let y: number = y0;
    let vy: number = vy0;
    for (let i = 1; i <= t; i++) {
        y += vy
        vy += -1
    }
    return y
}

const onTarget = (vx0: number, vy0: number, xLim: number[], yLim: number[]): number => {
    //let t = 0;
    let maxY = 0;
    let inside = false;
    for (let i = 0, len = 200; i < len; i++) {
        let x = xTrajectory(0, vx0, i);
        let y = yTrajectory(0, vy0, i);
        if (y > maxY) { maxY = y }
        if ((x >= xLim[0] && x <= xLim[1]) && (y >= yLim[0] && y <= yLim[1])) {
            //console.log(x, y);
            inside = true;
        }
    }
    return inside ? maxY : 0;

}


let maxY = 0;
let maxVY = 0;
let maxVX = 0;
for (let i = -300; i < 1000; i++) {
    for (let j = 0; j < 1000; j++) {
        let tmp_y = onTarget(i, j, xLim, yLim);
        if (tmp_y > maxY) {
            maxY = tmp_y;
            maxVY = j;
            maxVX = i;
        }
    }
}

console.log(maxY, maxVY, maxVX);
