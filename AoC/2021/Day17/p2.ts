import { readFileSync } from "fs";
import { join } from "path";
import _ from "lodash";

//const filename = 'example.txt'
const filename = "input.txt";

let input: string[][] = readFileSync(join(__dirname, filename))
    .toString()
    .trim()
    .split(": ")
    .map((line) => line.trim().split(", "));

let [xLim, yLim] = input[1].map((line) => line.slice(2).split("..").map(Number));
console.log(xLim, yLim);

const xTrajectory = (x0: number, vx0: number, t: number) => {
    let x: number = x0;
    let vx: number = vx0;
    for (let i = 1; i <= t; i++) {
        x += vx
        vx += vx > 0 ? -1 : vx < 0 ? 1 : 0;
    }
    return x
}
const yTrajectory = (y0: number, vy0: number, t: number) => {
    let y: number = y0;
    let vy: number = vy0;
    for (let i = 1; i <= t; i++) {
        y += vy
        vy += -1
    }
    return y
}

const onTarget = (vx0: number, vy0: number, xLim: number[], yLim: number[]): boolean => {
    let maxY = -Infinity;
    let inside = false;
    let passed = false;
    let i = 0;
    while (!passed) {
        let x = xTrajectory(0, vx0, i);
        let y = yTrajectory(0, vy0, i);
        if (y > maxY) { maxY = y }
        if ((x >= xLim[0] && x <= xLim[1]) && (y >= yLim[0] && y <= yLim[1])) {
            inside = true;
            passed = true;
        }
        if ((x > xLim[1]) || (y < yLim[0])) {
            passed = true;
        }
        i++

    }
    return inside;
}


//let goodCases: number[][] = [];
let total = 0;
for (let i = 0; i < 200; i++) {
    for (let j = -200; j < 200; j++) {
        if (onTarget(i, j, xLim, yLim)) {
            //goodCases.push([i, j])
            total++
        }
    }
}

console.log(total);
