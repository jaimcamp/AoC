import { readFileSync } from 'fs';

//const values:number[] = [199,
    //200,
    //208,
    //210,
    //200,
    //207,
    //240,
    //269,
    //260,
//263];

const values:number[] = readFileSync('Day01/input.txt', 'utf-8').toString().trim().split("\n").filter(Boolean).map(x => parseInt(x));

const lenVal:number = values.length;
const valuesShift = values.slice(1, lenVal);
console.log(valuesShift);

let timesDown:number = 0;
for (let i = 0, len = valuesShift.length; i < len; i++) {
    if (values[i] < valuesShift[i]) {
        timesDown++;
    }
}

console.log(timesDown);

