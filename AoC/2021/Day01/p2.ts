import { readFileSync } from 'fs';

//const values:number[] = [199,
    //200,
    //208,
    //210,
    //200,
    //207,
    //240,
    //269,
    //260,
//263];

const values:number[] = readFileSync('Day01/input.txt', 'utf-8').toString().trim().split("\n").filter(Boolean).map(x => parseInt(x));

const lenVal:number = values.length;
const windowSize:number = 3;

let sumWindow:number[] = [];

for (let i = 0, len = lenVal - windowSize + 1; i < len; i++) {
    sumWindow.push(
        values.slice(i, i+windowSize).reduce((a, b) => a+b)
    )
}
const lenWindow = sumWindow.length;
const valuesShift = sumWindow.slice(1, lenWindow);

let timesDown:number = 0;
for (let i = 0, len = valuesShift.length; i < len; i++) {
    if (sumWindow[i] < valuesShift[i]) {
        timesDown++;
    }
}

console.log(timesDown);

