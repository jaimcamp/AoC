import {readFileSync}  from 'fs';
import {join}  from 'path';
import * as _  from 'lodash-es';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input = readFileSync(join(__dirname, filename)).toString().trim().split(',').map(Number);



let days = 0;
const max_days = 256;
let state_counts = Array(9).fill(0);

input.forEach((fish_state) => state_counts[fish_state] += 1)

for (let i = 0, len = max_days; i < len; i++) {
    days++;
    let new_ones = state_counts.shift();
    state_counts[6] += new_ones;
    state_counts.push(new_ones);
}
console.log(days, state_counts, state_counts.reduce((a,b) => a+b))
