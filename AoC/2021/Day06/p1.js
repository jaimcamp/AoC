"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
//const filename:string = 'example.txt'
var filename = 'input.txt';
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename)).toString().trim().split(',').map(Number);
var days = 0;
var old_state = input;
var new_state = [];
var max_days = 256;
for (var i = 0, len = max_days; i < len; i++) {
    days++;
    for (var j = 0, len_1 = old_state.length; j < len_1; j++) {
        if (old_state[j] === 0) {
            new_state[j] = 6;
            new_state.push(8);
        }
        else {
            new_state[j] = old_state[j] - 1;
        }
    }
    old_state = new_state;
}
console.log(days, old_state, old_state.length);
