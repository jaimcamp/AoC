import {readFileSync}  from 'fs';
import {join}  from 'path';
import * as _  from 'lodash-es';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input = readFileSync(join(__dirname, filename)).toString().trim().split(',').map(Number);



let days = 0;
let old_state:number[] = input;
let new_state:number[]= [];
const max_days = 80;

for (let i = 0, len = max_days; i < len; i++) {
    days++;
    for (let j = 0, len = old_state.length; j < len; j++) {
        if (old_state[j] === 0){
            new_state[j] = 6;
            new_state.push(8);
        }
        else {
            new_state[j] = old_state[j] - 1;
        }
    }
    old_state = new_state;
}
console.log(days, old_state, old_state.length)
