"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
var lodash_1 = __importDefault(require("lodash"));
//const filename:string = 'example.txt'
var filename = 'input.txt';
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename)).toString().trim().split('\n').map(function (line) { return line.trim().split("").map(Number); });
var basins = Array(input.length).fill('').map(function () { return Array(input[0].length).fill(0); });
var lowPoints = [];
var isLow = function (point, neighbors) {
    var out = false;
    if (point < Math.min.apply(Math, neighbors)) {
        out = true;
    }
    return out;
};
var getNei = function (arr, y, x) {
    var out = [];
    var changes_y = [y + 1, y - 1];
    var changes_x = [x + 1, x - 1];
    for (var _i = 0, changes_y_1 = changes_y; _i < changes_y_1.length; _i++) {
        var change = changes_y_1[_i];
        if (change >= 0 && change < arr.length) {
            out.push(arr[change][x]);
        }
    }
    for (var _a = 0, changes_x_1 = changes_x; _a < changes_x_1.length; _a++) {
        var change = changes_x_1[_a];
        if (change >= 0 && change < arr[0].length) {
            out.push(arr[y][change]);
        }
    }
    return out;
};
for (var i = 0, len = input.length; i < len; i++) {
    for (var j = 0, len_1 = input[0].length; j < len_1; j++) {
        var adjLoc = getNei(input, i, j);
        if (isLow(input[i][j], adjLoc)) {
            lowPoints.push(input[i][j]);
            basins[i][j] = lowPoints.length;
        }
        if (input[i][j] === 9) {
            basins[i][j] = -1;
        }
    }
}
while (Math.max.apply(Math, basins.map(function (x) { return x.indexOf(0); })) != -1) {
    for (var i = 0, len = input.length; i < len; i++) {
        for (var j = 0, len_2 = input[0].length; j < len_2; j++) {
            if (basins[i][j] === 0) {
                var adjLoc = getNei(basins, i, j).filter(function (x) { return x != -1; });
                for (var _i = 0, adjLoc_1 = adjLoc; _i < adjLoc_1.length; _i++) {
                    var n = adjLoc_1[_i];
                    if (n != 0) {
                        basins[i][j] = n;
                    }
                }
            }
        }
    }
}
var basinsSize = lodash_1["default"].countBy(basins.flat());
basinsSize = lodash_1["default"].omit(basinsSize, '-1');
var sortedBasins = Object.entries(basinsSize).sort(function (_a, _b) {
    var a = _a[1];
    var b = _b[1];
    return b - a;
});
var total = 1;
for (var _a = 0, _b = sortedBasins.slice(0, 3); _a < _b.length; _a++) {
    var basin = _b[_a];
    total *= basin[1];
}
console.log(total);
