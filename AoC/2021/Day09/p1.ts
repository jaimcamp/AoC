import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split("").map(Number))

const lowPoints:number[] = [];
const isLow = (point:number, neighbors:number[]):boolean => {
    let out:boolean = false;
    if (point < Math.min(...neighbors)){
    out = true;
    }
    return out
}
const getNei = (arr:number[][], y:number, x:number):number[] => {
    // Try the 4 neighbor
    const out:number[] = [];
    //const y_plus = y+1;
    //const y_minus = y-1;
    //const x_plus = x+1;
    //const x_minus = x-1;
    const changes_y = [y+1, y-1];
    const changes_x = [x+1, x-1];
    for (const change of changes_y) {
        if (change >= 0 && change < arr.length) {
            out.push(arr[change][x])
        }
    }
    for (const change of changes_x) {
        if (change >= 0 && change < arr[0].length) {
            out.push(arr[y][change])
        }
    }
    //try {
        //out.push(arr[y+1][x])
    //} catch (e) {
        //[> handle error <]
    //}
    //try {
        //out.push(arr[y-1][x])
    //} catch (e) {
        //[> handle error <]
    //}
    //try {
        //out.push(arr[y][x+1])
    //} catch (e) {
        //[> handle error <]
    //}
    //try {
        //out.push(arr[y][x-1])
    //} catch (e) {
        //[> handle error <]
    //}
    return out
}

for (let i = 0, len = input.length; i < len; i++) {
    for (let j = 0, len = input[0].length; j < len; j++) {
        const adjLoc = getNei(input, i, j)
        if (isLow(input[i][j], adjLoc)){
            lowPoints.push(input[i][j])
        }
    }
}
console.log(lowPoints.map((x) => x+1).reduce((a,b) => a+b));
