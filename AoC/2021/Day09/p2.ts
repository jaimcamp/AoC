import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input:number[][] = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split("").map(Number))

const basins:number[][] = Array(input.length).fill('').map(function () { return Array<number>(input[0].length).fill(0)})
const lowPoints:number[] = [];
const isLow = (point:number, neighbors:number[]):boolean => {
    let out:boolean = false;
    if (point < Math.min(...neighbors)){
        out = true;
    }
    return out
}
const getNei = (arr:number[][], y:number, x:number):number[] => {
    const out:number[] = [];
    const changes_y = [y+1, y-1];
    const changes_x = [x+1, x-1];
    for (const change of changes_y) {
        if (change >= 0 && change < arr.length) {
            out.push(arr[change][x])
        }
    }
    for (const change of changes_x) {
        if (change >= 0 && change < arr[0].length) {
            out.push(arr[y][change])
        }
    }
    return out
}

for (let i = 0, len = input.length; i < len; i++) {
    for (let j = 0, len = input[0].length; j < len; j++) {
        const adjLoc = getNei(input, i, j)
        if (isLow(input[i][j], adjLoc)){
            lowPoints.push(input[i][j])
            basins[i][j] = lowPoints.length
        }
        if (input[i][j] === 9){ basins[i][j] = -1}
    }
}

while (Math.max(...basins.map((x) => x.indexOf(0))) != -1) {
    for (let i = 0, len = input.length; i < len; i++) {
        for (let j = 0, len = input[0].length; j < len; j++) {
            if (basins[i][j] === 0){
                const adjLoc = getNei(basins, i, j).filter((x) => x !=-1);
                for (const n of adjLoc) {
                    if (n != 0){
                        basins[i][j] = n;
                    }
                }
            }
        }
    }
}
let basinsSize = _.countBy(basins.flat())
basinsSize = _.omit(basinsSize, '-1')
const sortedBasins = Object.entries(basinsSize).sort( ([,a], [,b]) => b-a );
let total = 1;
for (const basin of sortedBasins.slice(0, 3)) {
    total *= basin[1]
}
console.log(total);
