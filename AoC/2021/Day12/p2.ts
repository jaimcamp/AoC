import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input:string[][] = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split("-"))
//const input:string[][] = [
    //'start-A',
    //'start-b',
    //'A-c',
    //'A-b',
    //'b-d',
    //'A-end',
    //'b-end'
//].map((line) => line.trim().split("-"))

const paths = new Map<string, string[]>();

for (let i = 0, len = input.length; i < len; i++) {
    const valueNode = paths.get(input[i][0]);
    if (valueNode == undefined) {
        paths.set(input[i][0], [input[i][1]])
    }
    else {
        valueNode.push(input[i][1])
    }
    const valueNodeEnd = paths.get(input[i][1]);
    if (valueNodeEnd == undefined) {
        paths.set(input[i][1], [input[i][0]])
    }
    else {
        valueNodeEnd.push(input[i][0])
    }
}

const oneLowerVisited = (visited:string[]):boolean => {
    return Object.values(_.countBy(visited.filter((x) => x.toLowerCase() == x))).some((x) => x>1)
}

const recursiveDiscovery = (currentNode:string, visitedNodes:string[]):string[][] => {
    visitedNodes = [...visitedNodes, currentNode]

    if (currentNode === 'end'){
        //console.log("Done");
        return [visitedNodes];
    }
    const output:string[][] = [];

    const children = paths.get(currentNode);
    if (children == undefined) {
        console.log(currentNode);
        throw Error(`undefined children ${currentNode}`);
    }
    for (const child of children) {
        if ((visitedNodes.includes(child) && child.toLowerCase() === child) && oneLowerVisited(visitedNodes) || child === 'start' ) {
            continue;
        }
        else {
            output.push(...recursiveDiscovery(child, visitedNodes))
        }
    }
    //console.log("Before returning");
    //console.table(output)
    return output;
}

console.table(recursiveDiscovery('start', []).length);
