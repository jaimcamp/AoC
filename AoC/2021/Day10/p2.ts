import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input:string[][] = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split(""))

//const openings:string[] = ['(', '[', '{', '<'];
const closings:string[] = [')', ']', '}', '>']

// Copied from somewhere deep
function median(numbers:number[]) {
    const sorted = numbers.slice().sort((a, b) => a - b);
    const middle = Math.floor(sorted.length / 2);

    if (sorted.length % 2 === 0) {
        return (sorted[middle - 1] + sorted[middle]) / 2;
    }

    return sorted[middle];
}

interface Point {
    [index:string]:number
}
interface Pairs {
    [index:string]:string
}

const brackets:Pairs = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}

const points:Point = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}

for (let i = 0, len = input.length; i < len; i++) {
    let again = true;
    while ( again ) {
        const toRemove:number[] = [];
        for (let j = 0, len = input[i].length - 1; j < len; j++) {
            if (brackets[input[i][j]] === input[i][j+1]){
                toRemove.push(...[j, j+1])
            }
        }
        input[i] = input[i].filter((e, idx, array) => toRemove.indexOf(idx) === -1)
        again = toRemove.length===0? false:true;
    }

}

const incomplete = input.filter((val) => _.intersection(closings, val).length === 0?true:false)

let score:number[] = [];
for (const line of incomplete) {
    let base = 0;
    for (const char of _.reverse(line)) {
        base = (base * 5) + points[brackets[char]]
    }
    score.push(base)
}

console.log(median(score));
