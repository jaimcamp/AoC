import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input:string[][] = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split(""))

//const openings:string[] = ['(', '[', '{', '<'];
const closings:string[] = [')', ']', '}', '>']

interface Point {
    [index:string]:number
}
interface Pairs {
    [index:string]:string
}

const brackets:Pairs = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}

const points:Point = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

for (let i = 0, len = input.length; i < len; i++) {
    let again = true;
    while ( again ) {
        const toRemove:number[] = [];
        for (let j = 0, len = input[i].length - 1; j < len; j++) {
            if (brackets[input[i][j]] === input[i][j+1]){
                toRemove.push(...[j, j+1])
            }
        }
        input[i] = input[i].filter((e, idx, array) => toRemove.indexOf(idx) === -1)
        again = toRemove.length===0? false:true;
    }

}

const worse:string[] =  [];
for (const line of input) {
    if (_.intersection(closings, line).length > 0){
        const bad = Math.min(...closings.map((val) => line.indexOf(val)).filter((val) => val>=0))
        worse.push(line[bad])
    }
}
let total = 0;
worse.forEach((val) => total += points[val[0]])
console.log(
    total
)
