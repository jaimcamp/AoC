import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

const util = require('util');

util.inspect.defaultOptions.maxArrayLength = null;

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input:number[][] = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split("").map(Number))
//const input:number[][] = [
    //'11111',
    //'19991',
    //'19191',
    //'19991',
    //'11111'
//].map((line) => line.trim().split("").map(Number))
let octopuses = _.cloneDeep(input);
console.table(octopuses);
const plus_one = (array:number[][]): number[][]  => {
    return array.map((line) => line.map((val) => val+1))
}

const flash_neighbors = (array:number[][], i:number, j:number) => {
    let neighbors = [
        [i, j+1],
        [i, j-1],
        [i+1, j],
        [i-1, j],
        [i+1, j+1],
        [i+1, j-1],
        [i-1, j+1],
        [i-1, j-1]
    ]
    neighbors = neighbors.filter((val) => val[0]>=0 && val[0] <array.length &&
        val[1] >= 0 && val[1] < array[0].length)
    for (const neighbor of neighbors) {
        array[neighbor[0]][neighbor[1]] = array[neighbor[0]][neighbor[1]] + 1
    }

    return array

}

const flashes = (array:number[][]): [number[][], number[][]] => {
    //let total_flashes = 0;
    const flashed:number[][] = []
    let run = true;
    while(run){
        for (let i = 0, len = array.length; i < len; i++) {
            for (let j = 0, len = array[i].length; j < len; j++) {
                if (array[i][j] > 9 && _.findIndex(flashed, function(x) {return x[0] == i && x[1] == j}) == -1){
                    //total_flashes++
                    flashed.push([i, j])
                    array = flash_neighbors(array, i, j)
                }
            }
        }
        const allNines:string[] = [];
        array.forEach((val, idx) => val.forEach(function(e, idy) {
            if(e>9) {
                allNines.push([idx, idy].join(""));
            }
        }
        ))
        if (_.difference( allNines,
            flashed.map((e) => e.join(""))).length == 0
        ) {
            run = false;

        }
    }
    return [array, flashed]
}

const back2Zero = (array:number[][]) => {
    array = array.map((line) => line.map((x) => x >9?0:x))
    return array
}

const allZero = (array:number[][]) => {
    return array.every(line => line.every(e => e == 0 ) == true)
}

let steps = 0;
const max_steps = 10000;
let flashed_total:number[][] = [];

while (steps < max_steps) {
    let run_flashes:number[][] = [];
    //console.table(octopuses);
    octopuses = plus_one(octopuses);
    //console.table(octopuses);
    [octopuses, run_flashes] = flashes(octopuses);
    //console.table(octopuses)
    //console.log(flashed_total);
    octopuses = back2Zero(octopuses);
    //console.table(octopuses);
    steps++
    flashed_total.push(...run_flashes)
    if (allZero(octopuses)){
        console.log(steps);
        break;
    }
}

