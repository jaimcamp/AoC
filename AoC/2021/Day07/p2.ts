import {readFileSync}  from 'fs';
import {join}  from 'path';
//import {min, max, range}  from 'lodash-es';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input = readFileSync(join(__dirname, filename)).toString().trim().split(',').map(Number);

// Modified from
// https://stackoverflow.com/questions/3959211/what-is-the-fastest-factorial-function-in-javascript/19139071
function sFact(num:number):number
{
    var rval=1;
    for (var i = 2; i <= num; i++)
        rval = rval + i;
    return rval;
}

const manhattan = (x0:number, x1:number):number => {
    return  sFact(Math.abs(x1-x0));
}

let positions = _.range(_.min(input)!, _.max(input)! + 1);

interface Distances {
    [distance: string] : number;
}

let total_distances: Distances = {};

for (const val of positions) {
    total_distances[val] = 0;
    for (let i = 0, len = input.length; i < len; i++) {
        total_distances[val] += manhattan(val, input[i]);
    }
}
const min_key = Object.keys(total_distances).reduce((prev, current) => total_distances[Number(current)] < total_distances[Number(prev)] ? current:prev)

console.log(total_distances[min_key])
