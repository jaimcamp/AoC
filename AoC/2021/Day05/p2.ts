import {readFileSync}  from 'fs';
import {join}  from 'path';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'
const input = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((element) => element.split(' -> ').map((e) => e.split(',').map(Number)))

function range(start:number, end:number, step:number = 1):number[] {
    const len = Math.floor((end - start) / step) + 1
    return Array(len).fill("").map((_, idx) => start + (idx * step))
}

const onlyStraightLines = (inputLine:number[][]):boolean => {
    let toFilter:boolean;
    if (inputLine[0][0] === inputLine[1][0] || inputLine[0][1] === inputLine[1][1]){
        toFilter = true;
    }
    else {
        toFilter = false;
    }
    return toFilter
}
const straightLines:number[][][] = input.filter(line => onlyStraightLines(line))
const diagonalLines:number[][][] = input.filter(line => ! onlyStraightLines(line))

const getPoints = (inputLine:number[][]):number[][] => {
    let [x1, y1] = inputLine[0];
    let [x2, y2] = inputLine[1];
    let outPoints:number[][] = []
    if (x1 === x2){
        let ys = range(Math.min(y1, y2), Math.max(y1, y2));
        for (const y of ys) {
            outPoints.push([x1, y])
        }
    }
    else if (y1 === y2){
        let xs = range(Math.min(x1, x2), Math.max(x1, x2));
        for (const x of xs) {
            outPoints.push([x, y1])
        }
    }
    else {
        const xChange:number = x1 < x2?1:-1;
        const yChange:number = y1 < y2?1:-1;
        for (let i = 0, len = Math.abs(x1-x2)+1; i < len; i++) {
            outPoints.push([
                x1 + (i * xChange),
                y1 + (i * yChange)
            ])
        }
    }
    return outPoints
}
const allPoints = input.flatMap((elements) => getPoints(elements))
console.log(allPoints)
const allPointsString = allPoints.map((e) => e.join(","))
const counts: {[key: string]: number} = {}
for (const num of allPointsString) {
    counts[num] = counts[num] ? counts[num] + 1 : 1;
}
console.log(Object.values(counts).filter(x => x>1).length)
