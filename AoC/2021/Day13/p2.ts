import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input:string[][] = readFileSync(join(__dirname, filename)).toString().trim().split("\n\n").map((line) => line.trim().split("\n"));


let pointsMarked:number[][] = _.cloneDeep(input[0].map((line) => line.split(",").map(Number)));
const instructions = input[1].map((line) => line.slice(11).split("="));

const max_y = Math.max(...pointsMarked.map((x) => x[1]))
const max_x = Math.max(...pointsMarked.map((x) => x[0]))

const foldPaper = (arr:number[][], axis:string, position:string):number[][] => {
    let out:number[][] = [];
    if (axis == 'x'){
        for (const point of arr) {
            if (point[0] > parseInt(position, 10)){
                const newPoint = [point[0] - parseInt(position, 10) -1 , point[1]]
                out.push(newPoint)
            }
            else {
                const newPoint = [(parseInt(position, 10) )  - point[0] -1, point[1]]
                out.push(newPoint)
            }
        }
    }
    if (axis == 'y'){
        for (const point of arr) {
            if (point[1] > parseInt(position, 10)){
                const newPoint = [point[0], parseInt(position,10)*2 - point[1] ]
                out.push(newPoint)
            }
            else {
                out.push(point)
            }
        }
    }
    return _.uniq(out.map((val) => val.join(","))).sort().map((val) => val.split(",").map(Number))
}

for (const instruction of instructions) {
    let [axis, position] = instruction
    pointsMarked = foldPaper(pointsMarked, axis, position);
    console.log(pointsMarked.length);

}
const f_max_y = Math.max(...pointsMarked.map((x) => x[1])) + 1
const f_max_x = Math.max(...pointsMarked.map((x) => x[0])) + 1

let canvas:string[][] = Array(f_max_y).fill("").map(() => Array(f_max_x).fill(" "))

for (const point of pointsMarked) {
    canvas[point[1]][point[0]] = "#"
}
console.table(canvas)
