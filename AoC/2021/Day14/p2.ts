import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

let input:string[][] = readFileSync(join(__dirname, filename)) .toString() .trim().split("\n\n").map((line) => line.trim().split("\n"));

let [a, b] = input;
let poly:string[] = a[0].split("");
let insertions:{[index:string]: string} = Object.fromEntries(b.map((line) => line.split(" -> ")));

let pairs:{[index:string]:number} = {}
Object.keys(insertions).forEach((val) => pairs[val] = 0)
for (let i = 0, len = poly.length -1 ; i < len; i++) {
    const pair = poly.slice(i, i+2).join("")
    pairs[pair]++
}

const getInserts = (arr:string[]) => {
    const out:{[index:number]:string} = {};
    for (let i = 0, len = arr.length -1 ; i < len; i++) {
        const pair = arr.slice(i, i+2).join("")
        out[i+1] = insertions[pair]
    }
    return out;
}

const insertNew = (arr:string[], toIns:{[index:number]:string}) => {
    for (const item of _.reverse(Object.keys(toIns)).map(Number)) {
        arr.splice(item, 0, toIns[item])
    }
    return arr
}

for (let i = 0, len = 40; i < len; i++) {
    //const newPairs = _.cloneDeep(pairs)
    let newPairs:{[index:string]:number} = {}
    Object.keys(insertions).forEach((val) => newPairs[val] = 0)
    for (const pair of Object.keys(pairs)) {
        if (pairs[pair] > 0){
            const amount  = pairs[pair];
            let newChar = insertions[pair];
            let x = pair.split("")
            let a = [x[0], newChar].join("");
            let b = [newChar, x[1]].join("");
            newPairs[a] += amount
            newPairs[b] += amount
            newPairs[pair] += -1 * amount
        }
    }
    for (const newVal of Object.keys(newPairs)){
        pairs[newVal] += newPairs[newVal]
    }
}

const countChars:{[index:string]:number} = { }
_.uniq(Object.keys(pairs).reduce((a, b) => a.concat(b))).forEach((e) => countChars[e] = 0)
for (const key of Object.keys(pairs)) {
    let amount = pairs[key];
    for (const letter of key.split("")) {
        countChars[letter] += amount;
    }
}

for (const letter of Object.keys(countChars)) {
    if ([poly[0], poly[poly.length -1]].includes(letter) ){
        countChars[letter]++
    }
    countChars[letter] = countChars[letter] / 2;
}

const sortCount = Object.entries(countChars).sort( ([,a], [,b]) => b-a );
console.log(sortCount);
const l = sortCount.length;
if (sortCount){
    const [t, first] =sortCount[0];
    const [r, last] = sortCount[l-1];
    console.log(first - last);

}

//console.log(poly.length);
//const sortCount = Object.entries(_.countBy(poly)).sort( ([,a], [,b]) => b-a );
//console.log(sortCount);
//const l = sortCount.length;
//if (sortCount){
    //const [t, first] =sortCount[0];
    //const [r, last] = sortCount[l-1];
    //console.log(first - last);

//}
