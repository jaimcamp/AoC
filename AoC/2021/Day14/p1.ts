import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

const filename:string = 'example.txt'
//const filename:string = 'input.txt'

let input:string[][] = readFileSync(join(__dirname, filename)) .toString() .trim().split("\n\n").map((line) => line.trim().split("\n"));

let [a, b] = input;
let poly:string[] = a[0].split("");
let insertions:{[index:string]: string} = Object.fromEntries(b.map((line) => line.split(" -> ")));

const getInserts = (arr:string[]) => {
    const out:{[index:number]:string} = {};
    for (let i = 0, len = arr.length -1 ; i < len; i++) {
        const pair = arr.slice(i, i+2).join("")
        out[i+1] = insertions[pair]
    }
    return out;
}

const insertNew = (arr:string[], toIns:{[index:number]:string}) => {
    for (const item of _.reverse(Object.keys(toIns)).map(Number)) {
        arr.splice(item, 0, toIns[item])
    }
    return arr
}

for (let i = 0, len = 10; i < len; i++) {
    let newChars = getInserts(poly);
    poly = insertNew(poly, newChars);
}
console.log(poly.length);
const sortCount = Object.entries(_.countBy(poly)).sort( ([,a], [,b]) => b-a );
console.log(sortCount);
const l = sortCount.length;
if (sortCount){
    const [t, first] =sortCount[0];
    const [r, last] = sortCount[l-1];
    console.log(first - last);

}
