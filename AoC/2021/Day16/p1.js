"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
exports.__esModule = true;
var lodash_1 = __importStar(require("lodash"));
var filename = 'example.txt';
//const filename:string = 'input.txt'
//const input:string = readFileSync(join(__dirname, filename)).toString().trim()
//const input: string = '38006F45291200';
//const input:string = 'EE00D40C823060';
//const input: string = '8A004A801A8002F478';
var input = '620080001611562C8802118E34';
//const input: string = 'C0015000016115A2E0802F182340';
//const input: string = 'A0016C880162017C3686B18A3D4780';
var decodeDict = {
    '0': '0000',
    '1': '0001',
    '2': '0010',
    '3': '0011',
    '4': '0100',
    '5': '0101',
    '6': '0110',
    '7': '0111',
    '8': '1000',
    '9': '1001',
    'A': '1010',
    'B': '1011',
    'C': '1100',
    'D': '1101',
    'E': '1110',
    'F': '1111'
};
var idLength = {
    '0': 15,
    '1': 11
};
var Packet = /** @class */ (function () {
    function Packet(code) {
        this.binCode = code;
        this.version = (0, lodash_1.parseInt)(code.slice(0, 3), 2);
        this.typeId = (0, lodash_1.parseInt)(code.slice(3, 6), 2);
        this.headerRest = code.slice(6);
        this.isLiteral = this.typeId === 4 ? true : false;
        this.body = {};
    }
    Packet.prototype.evalLit = function () {
        var groupStart = lodash_1["default"].range(0, this.headerRest.length, 5);
        var realNumber = "";
        var done = false;
        var end = this.headerRest.length - 1;
        for (var _i = 0, groupStart_1 = groupStart; _i < groupStart_1.length; _i++) {
            var start = groupStart_1[_i];
            if (!done) {
                var packet_1 = this.headerRest.substring(start, start + 5);
                if (packet_1[0] === '0') {
                    done = true;
                    end = start + 5;
                }
                realNumber += packet_1.substring(1);
            }
        }
        this.body.realNumber = (0, lodash_1.parseInt)(realNumber, 2);
        this.body.literalEnd = this.headerRest.substring(end);
    };
    Packet.prototype.evalOperator = function () {
        var lenType = idLength[this.headerRest[0]];
        var valueOperator = (0, lodash_1.parseInt)(this.headerRest.slice(1, lenType + 1), 2);
        var rest = this.headerRest.slice(lenType + 1);
        this.body.type = this.headerRest[0] === '0' ? 'length' : "amount";
        this.body.valueOperator = valueOperator;
        this.body.bodyRest = rest;
        this.body.arrayPackets = [];
        console.log("Operator", this.body);
        if (this.body.type === 'amount') {
            for (var i = 0, len = this.body.valueOperator; i < len; i++) {
                console.log("Amount", this.body.bodyRest);
                var child = new Packet(this.body.bodyRest);
                if (child.isLiteral) {
                    child.evalLit();
                    console.log(child);
                    this.body.arrayPackets.push(child);
                    this.body.bodyRest = child.body.literalEnd;
                }
                else {
                    child.evalOperator();
                    console.log(child);
                    this.body.arrayPackets.push(child);
                    this.body.bodyRest = child.body.literalEnd;
                }
            }
        }
        else {
            var startRest = this.body.bodyRest.length;
            while (this.body.bodyRest.length + this.body.valueOperator > startRest) {
                console.log("Length", this.body.bodyRest);
                var child = new Packet(this.body.bodyRest);
                if (child.isLiteral) {
                    child.evalLit();
                    console.log(child);
                    this.body.arrayPackets.push(child);
                    this.body.bodyRest = child.body.literalEnd;
                }
                else {
                    child.evalOperator();
                    console.log(child);
                    this.body.arrayPackets.push(child);
                    this.body.bodyRest = child.body.literalEnd;
                }
                console.log(this.body.bodyRest.length, this.body.valueOperator, startRest);
            }
        }
    };
    Packet.prototype.getVersion = function () {
        var totalVersion = this.version;
        if (this.body.arrayPackets != undefined) {
            for (var _i = 0, _a = this.body.arrayPackets; _i < _a.length; _i++) {
                var packet_2 = _a[_i];
                totalVersion += packet_2.getVersion();
            }
        }
        return totalVersion;
    };
    return Packet;
}());
var hex2bin = function (str) {
    return str.split("").map(function (x) { return decodeDict[x]; }).join("");
};
var packet = new Packet(hex2bin(input));
packet.isLiteral ? packet.evalLit() : packet.evalOperator();
console.log(input, packet, packet.getVersion(), packet.body.arrayPackets);
