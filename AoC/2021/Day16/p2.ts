import { readFileSync } from 'fs';
import { join } from 'path';
import _, { parseInt } from 'lodash';
import { version } from 'os';


//const filename: string = 'example.txt'
const filename:string = 'input.txt'

const input:string = readFileSync(join(__dirname, filename)).toString().trim()
//const input: string = 'C200B40A82';
//const input:string = '04005AC33890';
//const input: string = '880086C3E88112';
//const input: string = 'CE00C43D881120';
//const input: string = 'D8005AC2A8F0';
//const input: string = 'F600BC2D8F';
//const input: string = '9C005AC2F8F0';
//const input: string = '9C0141080250320F1802104A08';

const decodeDict: { [index: string]: string } = {
    '0': '0000',
    '1': '0001',
    '2': '0010',
    '3': '0011',
    '4': '0100',
    '5': '0101',
    '6': '0110',
    '7': '0111',
    '8': '1000',
    '9': '1001',
    'A': '1010',
    'B': '1011',
    'C': '1100',
    'D': '1101',
    'E': '1110',
    'F': '1111'
}
const idLength: { [index: string]: number } = {
    '0': 15,
    '1': 11
}

class Packet {
    //rawHex: string;
    binCode: string;
    version: number;
    typeId: number;
    headerRest: string;
    isLiteral: boolean;
    body: {
        realNumber?: number;
        literalEnd?: string;
        type?: string;
        valueOperator?: number;
        bodyRest?: string;
        arrayPackets?: Packet[];
    };

    constructor(code: string) {
        this.binCode = code;
        this.version = parseInt(code.slice(0, 3), 2);
        this.typeId = parseInt(code.slice(3, 6), 2);
        this.headerRest = code.slice(6);
        this.isLiteral = this.typeId === 4 ? true : false;
        this.body = {};
    }

    evalLit() {
        const groupStart: number[] = _.range(0, this.headerRest.length, 5);
        let realNumber: string = "";
        let done = false;
        let end: number = this.headerRest.length - 1;
        for (const start of groupStart) {
            if (done) { break }
            const packet = this.headerRest.substring(start, start + 5)
            realNumber += packet.substring(1)
            if (packet[0] === '0') { done = true; end = start+5 }

        }
        this.body.realNumber = parseInt(realNumber, 2);
        this.body.literalEnd = this.headerRest.slice(end) as string;
    }
    evalOperator() {
        const lenType = idLength[this.headerRest[0]];
        const valueOperator = parseInt(this.headerRest.slice(1, lenType + 1), 2);
        const rest = this.headerRest.slice(lenType + 1)
        const childrenValues:number[] = [];
        this.body.type = this.headerRest[0] === '0' ? 'length' : "amount";
        this.body.valueOperator = valueOperator;
        this.body.bodyRest = rest;

        this.body.arrayPackets = [];

        if (this.body.type === 'amount') {
            for (let i = 0, len = this.body.valueOperator; i < len; i++) {
                const child:Packet = new Packet(this.body.bodyRest!)
                if (child.isLiteral) {
                    child.evalLit()
                    this.body.arrayPackets.push(child)
                    this.body.bodyRest = child.body.literalEnd!;
                } else {
                    child.evalOperator()
                    this.body.arrayPackets.push(child)
                    this.body.bodyRest = child.body.bodyRest!;
                }
                childrenValues.push(child.body.realNumber!);
            }
        } else {
            const startRest = this.body.bodyRest.length;
            while (this.body.bodyRest.length + this.body.valueOperator > startRest){
                const child:Packet = new Packet(this.body.bodyRest!)
                if (child.isLiteral) {
                    child.evalLit()
                    this.body.arrayPackets.push(child)
                    this.body.bodyRest = child.body.literalEnd!;
                } else {
                    child.evalOperator()
                    this.body.arrayPackets.push(child)
                    this.body.bodyRest = child.body.bodyRest!;
                }
                childrenValues.push(child.body.realNumber!);
            }
        }
        console.log(childrenValues);
        switch (this.typeId) {
            case 0:
                this.body.realNumber = childrenValues.reduce((a,b)=> a+b)
                break;
            case 1:
                this.body.realNumber = childrenValues.reduce((a,b)=> a*b)
                break;
            case 2:
                this.body.realNumber = Math.min(...childrenValues)
                break;
            case 3:
                this.body.realNumber = Math.max(...childrenValues)
                break;
            case 5:
                this.body.realNumber = childrenValues[0] > childrenValues[1] ? 1 : 0;
                break;
            case 6:
                this.body.realNumber = childrenValues[0] < childrenValues[1] ? 1 : 0;
                break;
            case 7:
                this.body.realNumber = childrenValues[0] === childrenValues[1] ? 1 : 0;
                break;
            default:
        }


    }

    getVersion() {
        let totalVersion:number = this.version;
        if (this.body.arrayPackets != undefined){
            for (const packet of this.body.arrayPackets) {
                totalVersion += packet.getVersion();
            }
        }
        return totalVersion
    }

}

const hex2bin = (str: string): string => {
    return str.split("").map((x) => decodeDict[x]).join("")
}

let packet = new Packet(hex2bin(input));
packet.isLiteral ? packet.evalLit() : packet.evalOperator();

console.log(packet);
