import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split("|").map((v) => v.trim().split(" ")))

// Help
// 0: a, b, c, e, f, g
// 1: c, f *
// 2: a, c, d ,e, g
// 3: a, c, d, f, g
// 4: b, c, d, f *
// 5: a, b, d, f, g
// 6: a, b, d, e, f, g
// 7: a, c, f *
// 8: a, b, c, d, e, f , g *
// 9: a, b, c, d, f, g
let amount = 0;
const unique = [2, 3, 4, 7];
for (let i = 0, len = input.length; i < len; i++) {
   for (const digit of input[i][1]) {
       if (unique.indexOf(digit.length) +1 ) {
           console.log(digit, digit.length)
           amount++
       }
   }
}
console.log(amount)
