import {readFileSync}  from 'fs';
import {join}  from 'path';
import _ from 'lodash';

//const filename:string = 'example.txt'
const filename:string = 'input.txt'

const input = readFileSync(join(__dirname, filename)).toString().trim().split('\n').map((line) => line.trim().split("|").map((v) => v.trim().split(" ")))
//const input  = 'acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf'.trim().split('\n').map((line) => line.trim().split("|").map((v) => v.trim().split(" ")))

// Help
// 0: a, b, c, e, f, g
// 1: c, f *
// 2: a, c, d ,e, g
// 3: a, c, d, f, g
// 4: b, c, d, f *
// 5: a, b, d, f, g
// 6: a, b, d, e, f, g
// 7: a, c, f *
// 8: a, b, c, d, e, f , g *
// 9: a, b, c, d, f, g

interface Dic {
    [digit: string]: number;
}

const decipher = (line:string[]): Dic => {
    const lengths = line.map((v) => v.length)
    // 1 and 7 gives use top 'aaa'
    const one = line[lengths.indexOf(2)].split("").sort()
    const seven = line[lengths.indexOf(3)].split("").sort()
    const a = _.difference(seven, one)
    // 4 and 1 for possible 'b' and 'd'
    const four = line[lengths.indexOf(4)].split("").sort()
    const bd = _.difference(four, one)
    // 2, 3, 5 and 1 help us
    const idx235 = [];
    let tmp_235 = lengths.indexOf(5);
    while (tmp_235 != -1){
        idx235.push(tmp_235);
        tmp_235 = lengths.indexOf(5, tmp_235+1);
    }
    const twothreefive = _.at(line, idx235).map((v) => v.split("").sort())
    const twofive  = twothreefive.filter((x)=> _.intersection(x, one).length < 2)
    const three  = twothreefive.filter((x)=> _.intersection(x, one).length === 2)
    const be = _.difference( _.xor(twofive[0],twofive[1]), one)
    // and from 3 intersection with one and 'a' we get:
    const dg = _.difference(_.difference(three[0], one), a) ;
    // Clean up tuples
    const b = be.filter((x) => bd.indexOf(x) != -1)
    const d = bd.filter((x) => x!= b[0])
    const e = be.filter((x) => x!= b[0])
    const g = dg.filter((x) => x!= d[0])
    // Now for c and f using 6 and 9  and 0
    const idx069 = [];
    let tmp_069 = lengths.indexOf(6);
    while (tmp_069 != -1){
        idx069.push(tmp_069);
        tmp_069 = lengths.indexOf(6, tmp_069+1);
    }
    const zerosixnine = _.at(line, idx069).map((v) => v.split("").sort())
    const six  = zerosixnine.filter((x)=> _.intersection(x, one).length < 2)
    const f = six[0].filter((x) => one.indexOf(x[0]) != -1)
    const c = one.filter((x) => x!= f[0])

    const two = [...a, ...c, ...d, ...e, ...g].sort()
    const five = [...a, ...b, ...d, ...f, ...g].sort()
    const eight = [...a, ...b, ...c, ...d, ...e, ...f, ...g].sort()
    const nine = [...a, ...b, ...c, ...d, ...f, ...g].sort()
    const zero = [...a, ...b, ...c, ...e, ...f, ...g].sort()
    const out: Dic = { }
    out[one.join("")] = 1
    out[two.join("")] = 2
    out[three[0].join("")] = 3
    out[four.join("")] = 4
    out[five.join("")] = 5
    out[six[0].join("")] = 6
    out[seven.join("")] = 7
    out[eight.join("")] = 8
    out[nine.join("")] = 9
    out[zero.join("")] = 0

    return out


}

let total = 0;
for (let i = 0, len = input.length; i < len; i++) {
    let dec = decipher(input[i][0])
    const value:number[] = [];
    for (let j = 0, len = input[i][1].length; j < len; j++) {
        value.push(dec[input[i][1][j].split("").sort().join("")])
    }
    total += parseInt(value.join(""), 10)
}
console.log(total);
