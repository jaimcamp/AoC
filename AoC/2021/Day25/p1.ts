#!/usr/bin/env ts-node

import { readFileSync } from "fs";
import { join } from "path";
import * as _ from "lodash";

let filename: string = 'input.txt'
//let filename: string = 'example.txt'
let input = readFileSync(join(__dirname, filename))
    .toString()
    .trim()
    .split('\n')
    .map((line) => line.split(""))



let isMoving = true;
let currentState = _.cloneDeep(input)
const wLimit = input[0].length - 1;
const hLimit = input.length - 1;
console.log(wLimit, hLimit);

const moveRight = (arr: string[][], oldArr: string[][], j: number, i: number): string[][] => {
    //check next
    let iNext = i + 1;

    if (i === wLimit) {
        iNext = 0;
    }
    if (oldArr[j][iNext] === ".") {
        arr[j][iNext] = ">";
        arr[j][i] = ".";
    }
    return arr
}

const moveDown = (arr: string[][], oldArr: string[][], j: number, i: number): string[][] => {
    //check next
    let jNext = j + 1;

    if (j === hLimit) {
        jNext = 0;
    }
    if (oldArr[jNext][i] === ".") {
        arr[jNext][i] = "v";
        arr[j][i] = ".";
    }
    return arr
}

let step = 1;
while (isMoving) {
    let oldState = _.cloneDeep(currentState)
    //console.table(oldState);
    for (let i = 0, len = currentState[0].length; i < len; i++) {
        for (let j = 0, len = currentState.length; j < len; j++) {
            if (oldState[j][i] === '>') {
                currentState = moveRight(currentState, oldState, j, i)
            }
        }
    }
    let midState = _.cloneDeep(currentState)
    for (let i = 0, len = currentState[0].length; i < len; i++) {
        for (let j = 0, len = currentState.length; j < len; j++) {
            if (midState[j][i] == 'v') {
                currentState = moveDown(currentState, midState, j, i)
            }
        }
    }


    if (JSON.stringify(oldState) === JSON.stringify(currentState)) {
        isMoving = false;
        console.log(step);
    }
    step++;
}
