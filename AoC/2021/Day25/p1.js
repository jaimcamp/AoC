#!/usr/bin/env ts-node
"use strict";
exports.__esModule = true;
var fs_1 = require("fs");
var path_1 = require("path");
var _ = require("lodash");
var filename = 'input.txt';
//let filename: string = 'example.txt'
var input = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, filename))
    .toString()
    .trim()
    .split('\n')
    .map(function (line) { return line.split(""); });
var isMoving = true;
var currentState = _.cloneDeep(input);
var wLimit = input[0].length - 1;
var hLimit = input.length - 1;
console.log(wLimit, hLimit);
var moveRight = function (arr, oldArr, j, i) {
    //check next
    var iNext = i + 1;
    if (i === wLimit) {
        iNext = 0;
    }
    if (oldArr[j][iNext] === ".") {
        arr[j][iNext] = ">";
        arr[j][i] = ".";
    }
    return arr;
};
var moveDown = function (arr, oldArr, j, i) {
    //check next
    var jNext = j + 1;
    if (j === hLimit) {
        jNext = 0;
    }
    if (oldArr[jNext][i] === ".") {
        arr[jNext][i] = "v";
        arr[j][i] = ".";
    }
    return arr;
};
var step = 1;
while (isMoving) {
    var oldState = _.cloneDeep(currentState);
    //console.table(oldState);
    for (var i = 0, len = currentState[0].length; i < len; i++) {
        for (var j = 0, len_1 = currentState.length; j < len_1; j++) {
            if (oldState[j][i] === '>') {
                currentState = moveRight(currentState, oldState, j, i);
            }
        }
    }
    var midState = _.cloneDeep(currentState);
    for (var i = 0, len = currentState[0].length; i < len; i++) {
        for (var j = 0, len_2 = currentState.length; j < len_2; j++) {
            if (midState[j][i] == 'v') {
                currentState = moveDown(currentState, midState, j, i);
            }
        }
    }
    if (JSON.stringify(oldState) === JSON.stringify(currentState)) {
        isMoving = false;
        console.log(step);
    }
    step++;
}
